@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-pickadate.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.schedule.main_stage.index') }}">Schedule Attachment</a>
						</li>
						<li class="breadcrumb-item active">
							@if ($data !== null)
							Update Schedule Attachment
							@else
							Create Schedule Attachment
							@endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		@if ($data !== null)
		<form action="{{ route('admin.schedule.main_stage.attachment.update', $data->id) }}" method="POST" enctype="multipart/form-data">        
		@else
		<form action="{{ route('admin.schedule.main_stage.attachment.store', $main_stage->id) }}" id="jquery-val-form" method="POST" enctype="multipart/form-data">
		@endif
		@csrf
		<div class="row">
			<!-- Bootstrap Validation -->
			<div class="col-md-12 col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Schedule Attachment</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Type</label>
							<div class="demo-inline-spacing">
								<div class="custom-control custom-radio">
									<input type="radio" value="document" id="customRadio1" name="customRadio" class="custom-control-input" {{ $data ? $data->type =='document' ? 'checked' : '' : '' }} />
									<label class="custom-control-label" for="customRadio1">Document</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio2" value="image" name="customRadio" class="custom-control-input"  {{ $data ? $data->type =='image' ? 'checked' : '' : '' }} />
									<label class="custom-control-label" for="customRadio2">Image</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio3" value="video" name="customRadio" class="custom-control-input"  {{ $data ? $data->type =='video' ? 'checked' : '' : '' }} />
									<label class="custom-control-label" for="customRadio3">Video</label>
								</div>
							</div>
							@error('inlineRadioOptions')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div id="type-attachment">
							@if($data)
							@if($data->type == 'document')
							<div class="form-group">
								<div class="my-2">
									<a href="/file/schedule/{{ $data ? $data->file : '' }}">Download : {{ $data ? $data->file : '' }}</a>
								</div>
								<label class="form-label" for="basic-addon-name">File</label>

								

								<input type="file" id="name" name="file" class="form-control @error('file') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->file : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* File types support: pdf,docx,odt,xls,xlsx,pptx with maximum size 2MB</small>
								@error('file')
								<div>
									
								<span class="text-danger">{{ $message }}</span>
								</div>
								@enderror
							</div>
							@elseif($data->type == 'image')
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Image</label>

								<input type="file" id="name" name="file" class="form-control @error('file') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->file : '' }}" aria-describedby="basic-addon-name" onchange="loadFile(event)"/>
								<small class="text-danger">* File types support: jpg,jpeg,png with maximum size 1MB Square or Landscape dimensions</small>
								@error('file')
								<div>
									
								<span class="text-danger">{{ $message }}</span>
								</div>
								@enderror
								<div>
									<img
                                    id="foto_profile"
                                    class="m-2"
                                    style="width:30%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;"
                                    src="/img/schedule/{{ $data ? $data->file : 'default.jpg'}}">
								</div>
							</div>
							@else
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Video Short Name</label>

								<input type="text" id="name" name="video_name" class="form-control @error('video_name') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->video_name : '' }}" aria-describedby="basic-addon-name" />
								@error('video_name')
								<span class="text-danger">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Youtube Url</label>

								<input type="text" id="name" name="youtube_url" class="form-control @error('youtube_url') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->youtube_url : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</small>
								@error('youtube_url')
								<span class="text-danger">{{ $message }}</span>
								@enderror
							</div>
							@endif
							@else
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">File</label>

								<input type="file" id="name" name="file" class="form-control @error('file') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->file : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* File types support: pdf,docx,odt,xls,xlsx,pptx with maximum size 2MB</small>
								@error('file')
								<div>
									
								<span class="text-danger">{{ $message }}</span>
								</div>
								@enderror
							</div>
							@endif
							

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mb-4">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</section>
</div>

@endsection

@section('script')
<script type="text/javascript">
	$('.custom-control-input').on('click', function () {
		var type = $(this).val();
		if (type == 'document') {
			$('#type-attachment').html(`
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">File</label>

								<input type="file" id="name" name="file" class="form-control @error('file') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->file : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* File types support: pdf,docx,odt,xls,xlsx,pptx with maximum size 2MB</small>
								@error('file')
								<div>
									
								<span class="text-danger">{{ $message }}</span>
								</div>
								@enderror
							</div>`);
		} else if(type == 'image') {
			$('#type-attachment').html(`
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Image</label>

								<input type="file" id="name" name="file" class="form-control @error('file') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->file : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* File types support: jpg,jpeg,png with maximum size 1MB Square or Landscape dimensions</small>
								@error('file')
								<div>
									
								<span class="text-danger">{{ $message }}</span>
								</div>
								@enderror
							</div>`);
		} else {
			$('#type-attachment').html(`
				<div class="form-group">
								<label class="form-label" for="basic-addon-name">Video Short Name</label>

								<input type="text" id="name" name="video_name" class="form-control @error('video_name') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->video_name : '' }}" aria-describedby="basic-addon-name" />
								@error('video_name')
								<span class="text-danger">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Youtube Url</label>

								<input type="text" id="name" name="youtube_url" class="form-control @error('youtube_url') is-invalid @enderror" aria-label="Name" value="{{ $data ? $data->youtube_url : '' }}" aria-describedby="basic-addon-name" />
								<small class="text-danger">* Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</small>
								@error('youtube_url')
								<span class="text-danger">{{ $message }}</span>
								@enderror
							</div>
							`);
		}
	});
</script>
@endsection