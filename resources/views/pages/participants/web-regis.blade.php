@extends('layouts.app')
@section('content')

<div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Participant</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Participant Web Regisration</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic table -->
            <section id="ajax-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h4 class="card-title">Participant Web Regisration</h4>
                                <a href="{{ route('admin.participant.create') }}" class="btn btn-primary">Add Data</a>
                            </div>
                            <div class="card-datatable">
                                <table class="table" id="data-participant">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Ticket</th>
                                            <th>Name</th>
                                            <th>Schedule</th>
                                            <th>Company</th>
                                            <th>City</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Basic table -->


        </div>
    </div>

    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/app-assets/js/scripts/ui/ui-feather.js" defer></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/js/scripts/components/components-dropdowns.js" defer></script>
    <script src="/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js" defer></script>
    <script src="/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js" defer></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" defer></script>
    <script src="/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js" defer></script>

    <script src="/app-assets/js/scripts/tables/table-datatables-advanced.js" defer></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    @if (session('created'))
        <script>
            $(function(){
                toastr['success']('👋 Yes Data Created !!.', 'Created Success', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @elseif(session('updated'))
        <script>
            $(function(){
                toastr['success']('👋 Yes Data Updated !!.', 'Updated Success', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @endif

    <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-participant').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.participant.web-regis') }}",
                columns: [
                    {
                        data: null, "sortable": false,
                        render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; },
                    },
                    {
                        data: 'ticket_id',
                        name: 'ticket_id',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'schedule_id',
                        name: 'schedule_id',
                    },
                    {
                        data: 'company',
                        name: 'company',
                    },
                    {
                        data: 'city_id',
                        name: 'city_id',
                    },
                    {
                        data: 'id',
                        render: (id) => /* html */`
                            <a href="/admin/participant/update/${id}" class="btn text-warning dropdown-item"></i>Update</a>
                            <a class="dropdown-item btn text-danger" onclick="hapus(${id})"></i>Delete</a>
                            <a href="/admin/participant/detail/${id}" class="btn text-info dropdown-item"></i>Detail</a>
                        `
                    },
                ],
                columnDefs: [
                    {
                    className: 'control',
                    orderable: false,
                    targets: 0
                    }
                ],
                dom:
                    '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                orderCellsTop: true,
                responsive: {
                    details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                        var data = row.data();
                        return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                    }
                },
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                },
                drawCallback: () => {
                    $('.delete').click(function () {
                    const id = $(this).data(id)
                    })
                }
            });
            
        });

        function hapus(id){
            clearToastObj = toastr['error'](
                'Are You Delete?<br /><br /><button type="button" class="btn btn-danger btn-sm delete">Yes</button>',
                'Deleted',
                {
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0,
                tapToDismiss: false,
                }
            );

            if (clearToastObj.find('.delete').length) {
                clearToastObj.delegate('.delete', 'click', function () {
                    toastr.clear(clearToastObj, { force: true });
                    clearToastObj = undefined;
                    $.ajax({
                        method: "GET",
                        url: "/admin/participant" + '/' + id,
                        success: function (data) {
                            toastr['success']('👋 Yes Data Deleted !!.', 'Progress Bar', {
                                closeButton: true,
                                tapToDismiss: false,
                                progressBar: true,
                            });
                            location.reload();
                        },
                        error: function (data) {
                            toastr['success']('👋 Chocolate oat cake jelly oat cake candy jelly beans pastry.', 'Progress Bar', {
                                closeButton: true,
                                tapToDismiss: false,
                                progressBar: true,
                                rtl: isRtl
                            });
                        }
                    });
                });
            }
        }
    </script>
@endsection