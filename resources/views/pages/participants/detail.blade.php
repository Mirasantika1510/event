@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="/app-assets/css/uploader.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Participant</a>
						</li>
						<li class="breadcrumb-item active">
                            Detail Participant
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        <div class="row">
            <!-- User Card starts-->
            <div class="col-xl-9 col-lg-8 col-md-7">
                <div class="card user-card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg" style="margin-top: 22px;">
                                <div class="user-avatar-section">
                                    <div class="d-flex justify-content-start">
                                        <img class="img-fluid rounded" src="/img/participant/{{ $participant->image }}" height="104" width="104" alt="User avatar" />
                                        <div class="d-flex flex-column ml-1">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">{{ $participant->name }}</h4>
                                                <span class="card-text">{{ $participant->email }}</span>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                <a href="{{ route('admin.participant.edit',$participant->id) }}" class="btn btn-warning">Edit</a>
                                                <a href="{{ route('admin.participant.web-regis') }}" class="btn btn-outline-primary ml-1">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                <div class="user-info-wrapper">
                                    <div class="d-flex flex-wrap">
                                        <div class="user-info-title">
                                            <i data-feather="user" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">Email : </span>
                                        </div>
                                        <p class="card-text mb-0">{{ $participant->email }}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="star" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">Company : </span>
                                        </div>
                                        <p class="card-text mb-0">{{ $participant->company }}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="star" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">Jobs Title : </span>
                                        </div>
                                        <p class="card-text mb-0">{{ $participant->jobs }}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="flag" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">City : </span>
                                        </div>
                                        <p class="card-text mb-0">{{ $participant->city->nama_kota }}</p>
                                    </div>
                                    <div class="d-flex flex-wrap">
                                        <div class="user-info-title">
                                            <i data-feather="phone" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">Phone : </span>
                                        </div>
                                        <p class="card-text mb-0">{{ $participant->phone }}</p>
                                    </div>
                                    <div class="d-flex flex-wrap">
                                        <div class="user-info-title">
                                            <i data-feather='download' class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">Type : </span>
                                        </div>
                                        <p class="card-text mb-0">
                                            @if ($participant->type == 1)
                                                Web Registration
                                            @else
                                                Invtation
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /User Card Ends-->

            <!-- Plan Card starts-->
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="card plan-card border-{{ $participant->payment->status == 'success' ? 'success' : ''}}{{ $participant->payment->status == 'waiting_confirmation' ? 'warning' : ''}}{{ $participant->payment->status == 'waiting_payment' ? 'danger' : ''}}">
                    <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                        <h5 class="mb-0">Payment</h5>
                        <span class="badge badge-light-secondary" data-toggle="tooltip" data-placement="top" title="Payment Date">{{ \Carbon\Carbon::parse($participant->payment->created_at)->format('M d') }}, <span class="nextYear"></span>
                        </span>
                    </div>
                    <div class="card-body">
                        @if ($participant->payment->status == 'success')
                            <div class="badge badge-light-success">Success</div>
                        @elseif($participant->payment->status == 'waiting_confirmation')
                            <div class="badge badge-light-warning">Waiting Confirmation</div>
                        @else
                            <div class="badge badge-light-danger">Waiting Payment</div>
                        @endif
                        <ul class="list-unstyled my-1">
                            <li>
                                <span class="align-middle">Schedule : {{ $participant->schedule->title }}</span>
                            </li>
                            <li class="my-25">
                                <span class="align-middle">Ticket : {{ $participant->ticket->name }}</span>
                            </li>
                            {{-- <li>
                                <span class="align-middle">Basic Support</span>
                            </li> --}}
                        </ul>
                        {{-- <button class="btn btn-primary text-center btn-block">Upgrade Plan</button> --}}
                    </div>
                </div>
            </div>
            <!-- /Plan CardEnds -->
        </div>
	</section>
</div>

@endsection