@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="/app-assets/css/uploader.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Participant</a>
						</li>
						<li class="breadcrumb-item active">
                            @if ($data !== null)
                                Update Participant
                            @else
                                Create Participant
                            @endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        @if ($data !== null)
		    <form action="{{ route('admin.participant.update',$data->id) }}" method="POST" enctype='multipart/form-data'>        
        @else
		    <form action="{{ route('admin.participant.store') }}" id="jquery-val-form" method="POST" enctype='multipart/form-data'>
        @endif
            @csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Participant Category</h4>
						</div>
						<div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" id="name" class="form-control @error('name') border-danger @enderror" placeholder="Input Name ..." value="{{ $data ? $data->name : '' }}" name="name" />
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="company">Company</label>
                                        <input type="text" id="company" class="form-control @error('company') border-danger @enderror" placeholder="Input Company ..." value="{{ $data ? $data->company : '' }}" name="company" />
                                        @error('company')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="jobs">Job Title</label>
                                        <input type="text" id="jobs" class="form-control @error('jobs') border-danger @enderror" placeholder="Input Job Title ..." value="{{ $data ? $data->jobs : '' }}" name="jobs" />
                                        @error('jobs')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="phone">Phone</label>
                                        <input type="number" id="phone" class="form-control @error('phone') border-danger @enderror" placeholder="Input Number Phone ..." value="{{ $data ? $data->phone : '' }}" name="phone" />
                                        @error('phone')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="select2 form-control form-control-lg @error('city_id') border-danger @enderror" name="city_id">
                                            <option>Please a Select City</option>
                                            @foreach ($city as $item)
                                                @if ($data !== null)
                                                    <option value="{{ $item->id }}" {{ $item->id == $data->city_id ? 'selected' : ''}}>{{ $item->nama_kota }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{{ $item->nama_kota }}</option>
                                                @endif
                                            @endforeach
                                            @error('city_id')
                                                <span class="text-danger">Please Select City</span>
                                            @enderror
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="email">Email</label>
                                        <input type="text" id="email" class="form-control @error('email') border-danger @enderror" placeholder="Input Email ..." value="{{ $data ? $data->email : '' }}" name="email" />
                                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    @if ($data == null)
                                        <div class="form-group">
                                            <label class="form-label" for="password">Password</label>
                                            <input type="password" id="password" class="form-control @error('password') border-danger @enderror" placeholder="Input Password ..." value="{{ $data ? $data->password : '' }}" name="password" />
                                            @error('password')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    @endif
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Ticket</label>
                                        <select class="select2 form-control form-control-lg @error('ticket_id') border-danger @enderror" name="ticket_id">
                                            <option>Please a Select Ticket</option>
                                            @foreach ($ticket as $item)
                                                @if ($data !== null)
                                                    <option value="{{ $item->id }}" {{ $item->id == $data->ticket_id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('ticket_id')
                                            <span class="text-danger">Please Select Ticket</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Schedule</label>
                                        <select class="select2 form-control form-control-lg @error('schedule_id') border-danger @enderror" name="schedule_id[]" multiple>
                                            @foreach ($scedule as $item)
                                                @if ($data !== null)
                                                    <option value="{{ $item->id }}" {{ $item->id == $data->schedule_id ? 'selected' : '' }}>{{ $item->title }}</option>                                                    
                                                @else
                                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('schedule_id')
                                            <span class="text-danger">Please Select Schedule</span>
                                        @enderror
                                    </div>
                                    <div class="file-upload  @error('image') border-danger @enderror" style="width: 103%;">
                                        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add image</button>
                                        <div class="image-upload-wrap">
                                            <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" name="image" />
                                            <div class="drag-text">
                                                @if ($data !== null)
                                                    <img class="file-upload-image" src="/img/participant/{{ $data ? $data->image : '' }}" alt="your image" />
                                                @else
                                                    <h3>Drag and drop a file or select add image</h3>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="file-upload-content">
                                            <img class="file-upload-image" src="/img/participant/{{ $data ? $data->image : '' }}" alt="your image" />
                                            <div class="image-title-wrap">
                                                <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded image</span></button>
                                            </div>
                                        </div>
                                    </div>
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    @if ($data !== null)
                                        @if ($data->type == '1')
                                            <div class="form-group">
                                                <label>Type</label>
                                                <select class="form-control form-control-lg" name="type">
                                                    <option value="1">Web Registration</option>
                                                </select>
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label>Type</label>
                                                <select class="form-control form-control-lg" name="type">
                                                    <option value="2">Invatation</option>
                                                </select>
                                            </div>
                                        @endif
                                    @else
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control form-control-lg" name="type">
                                                <option disabled>Please a Select Type</option>
                                                <option value="1">Web Registration</option>
                                                <option value="2">Invatation</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<script src="/app-assets/vendors/js/extensions/dropzone.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/form-file-uploader.js" defer></script>
<script src="/app-assets/vendors/js/forms/select/select2.full.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/form-select2.js" defer></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $(function (){
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });

        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });
    });
</script>

@endsection