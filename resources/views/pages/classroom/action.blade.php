@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-pickadate.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.schedule.classroom.index') }}">Main Stage</a>
						</li>
						<li class="breadcrumb-item active">
							@if ($data !== null)
							Update Main Stage
							@else
							Create Main Stage
							@endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		@if ($data !== null)
		<form action="{{ route('admin.schedule.classroom.update', $data->id) }}" method="POST" enctype="multipart/form-data">        
		@else
		<form action="{{ route('admin.schedule.classroom.store') }}" id="jquery-val-form" method="POST" enctype="multipart/form-data">
		@endif
		@csrf
		<div class="row">
			<!-- Bootstrap Validation -->
			<div class="col-md-12 col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Main Stage</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Title</label>

							<input type="text" id="name" name="name" class="form-control" aria-label="Name" value="{{ $data ? $data->title : '' }}" aria-describedby="basic-addon-name" required />
							<small class="text-danger">* Maximum length : 150 characters</small>
							@error('name')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Thumbnail</label>

							<input type="file" id="name" name="thumbnail" class="form-control" aria-label="Name" value="{{ $data ? $data->thumbnail : '' }}" aria-describedby="basic-addon-name" />
							<small class="text-danger">* Max size 100KB Square dimensions</small>
							@error('thumbnail')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label class="form-label" for="basic-addon-name">Date</label>

									<input type="date" id="date" name="date" class="form-control" aria-label="Name" value="{{ $data ? $data->date : '' }}" aria-describedby="basic-addon-name" required />
									@error('date')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
								<div class="col-md-4">
									<label class="form-label" for="basic-addon-name">Time Start</label>

									<input type="time" id="time_start" name="time_start" class="form-control" aria-label="Name" value="{{ $data ? $data->time_start : '' }}" aria-describedby="basic-addon-name" required />
									@error('time_start')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
								<div class="col-md-4">
									<label class="form-label" for="basic-addon-name">Time End</label>

									<input type="time" id="time_end" name="time_end" class="form-control" aria-label="Name" value="{{ $data ? $data->time_end : '' }}" aria-describedby="basic-addon-name" required />
									@error('time_end')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Description</label>
							<div id="snow-wrapper">
								<div id="snow-container">
									<div class="quill-toolbar">
										<span class="ql-formats">
											<select class="ql-header">
												<option value="1">Heading</option>
												<option value="2">Subheading</option>
												<option selected>Normal</option>
											</select>
											<select class="ql-font">
												<option selected>Sailec Light</option>
												<option value="sofia">Sofia Pro</option>
												<option value="slabo">Slabo 27px</option>
												<option value="roboto">Roboto Slab</option>
												<option value="inconsolata">Inconsolata</option>
												<option value="ubuntu">Ubuntu Mono</option>
											</select>
										</span>
										<span class="ql-formats">
											<button class="ql-bold"></button>
											<button class="ql-italic"></button>
											<button class="ql-underline"></button>
										</span>
										<span class="ql-formats">
											<button class="ql-list" value="ordered"></button>
											<button class="ql-list" value="bullet"></button>
										</span>
										<span class="ql-formats">
											<button class="ql-link"></button>
											<button class="ql-image"></button>
											<button class="ql-video"></button>
										</span>
										<span class="ql-formats">
											<button class="ql-formula"></button>
											<button class="ql-code-block"></button>
										</span>
										<span class="ql-formats">
											<button class="ql-clean"></button>
										</span>
									</div>
									<div class="editor">
										<textarea name="description" id="snow-wrapper" class="form-control @error('description') border-danger @enderror" cols="30" rows="10">{{ $data ? $data->description : '' }}</textarea>
									</div>
									
									@error('description')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Pinned Message</label>

							<input type="text" id="pinned_message" name="pinned_message" class="form-control" aria-label="Name" value="{{ $data ? $data->pinned_message : '' }}" aria-describedby="basic-addon-name" required />
							<small class="text-danger">* Maximum length: 255 characters</small>
							@error('pinned_message')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label class="form-label" for="basic-addon-name">Type</label>

									<input type="type" id="type" name="type" class="form-control" aria-label="Name" value="{{ $data ? $data->type == 'classroom' ? 'Classroom' : '' : 'Classroom' }}" aria-describedby="basic-addon-name" readonly="" />
									@error('type')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
								<div class="col-md-6">
									<label class="form-label" for="basic-addon-name">Platform Type</label>

									<select class="form-control" id="select-country1" name="platform_type" required>
										@if($data)
										@if($data->platform_type == 'youtube')
										<option value="youtube">Youtube</option>
										<option value="zoom">Zoom</option>
										@else
										<option value="zoom">Zoom</option>
										<option value="youtube">Youtube</option>
										@endif
										@else
										<option value="youtube">Youtube</option>
										<option value="zoom">Zoom</option>
										@endif
									</select>
									@error('platform_type')
									<span class="text-danger">{{ $message }}</span>
									@enderror
								</div>
							</div>
						</div>
						<div id="platform-type">
							@if($data)
							@if($data->platform_type == 'youtube')
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Url</label>

										<input type="text" id="youtube_url" name="youtube_url" class="form-control" aria-label="Name" value="{{ $data ? $data->youtube_url : '' }}" aria-describedby="basic-addon-name" required />
										<small class="text-danger">*Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</small>
										@error('youtube_url')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Playlist</label>

										<div class="custom-control custom-radio my-50">
											<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->youtube_playlist == 1 ? 'checked' : '' : '' }} />
											<label class="custom-control-label" for="validationRadio3">Yes</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->youtube_playlist == 0 ? 'checked' : '' : '' }}/>
											<label class="custom-control-label" for="validationRadio4">No</label>
										</div>
										@error('youtube_playlist')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							@else
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Conference Id </label>
										<input type="text" id="zoom_id" name="zoom_id" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_id : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_id')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Conference Password</label>
										<input type="text" id="zoom_password" name="zoom_password" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_password : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_password')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Api Key</label>
										<input type="text" id="zoom_api_key" name="zoom_api_key" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_api_key : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_api_key')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Api Secret</label>
										<input type="text" id="zoom_api_secret" name="zoom_api_secret" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_api_secret : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_api_secret')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Link</label>
										<input type="text" id="zoom_url" name="zoom_url" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_url : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_url')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							@endif
							@else
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Url</label>

										<input type="text" id="youtube_url" name="youtube_url" class="form-control" aria-label="Name" value="{{ $data ? $data->youtube_url : '' }}" aria-describedby="basic-addon-name" required />
										<small class="text-danger">*Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</small>
										@error('youtube_url')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Playlist</label>

										<div class="custom-control custom-radio my-50">
											<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->youtube_playlist == 1 ? 'checked' : '' : '' }} />
											<label class="custom-control-label" for="validationRadio3">Yes</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->youtube_playlist == 0 ? 'checked' : '' : '' }}/>
											<label class="custom-control-label" for="validationRadio4">No</label>
										</div>
										@error('youtube_playlist')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							@endif
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row mb-4">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</section>
</div>

@endsection

@section('script')
<script type="text/javascript">
	$('#select-country1').on('change', function () {
		var type = $('#select-country1').val();
		if (type == 'youtube') {
			$('#platform-type').html(`
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Url</label>

										<input type="text" id="youtube_url" name="youtube_url" class="form-control" aria-label="Name" value="{{ $data ? $data->youtube_url : '' }}" aria-describedby="basic-addon-name" required />
										<small class="text-danger">*Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</small>
										@error('youtube_url')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									<div class="col-md-6">
										<label class="form-label" for="basic-addon-name">Youtube Playlist</label>
										
										<div class="custom-control custom-radio my-50">
											<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->youtube_playlist == 1 ? 'checked' : '' : '' }} />
											<label class="custom-control-label" for="validationRadio3">Yes</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->youtube_playlist == 0 ? 'checked' : '' : '' }}/>
											<label class="custom-control-label" for="validationRadio4">No</label>
										</div>
										@error('youtube_playlist')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>`);
		} else {
			$('#platform-type').html(`
				<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Conference Id </label>
										<input type="text" id="zoom_id" name="zoom_id" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_id : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_id')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Conference Password</label>
										<input type="text" id="zoom_password" name="zoom_password" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_password : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_password')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Api Key</label>
										<input type="text" id="zoom_api_key" name="zoom_api_key" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_api_key : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_api_key')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Api Secret</label>
										<input type="text" id="zoom_api_secret" name="zoom_api_secret" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_api_secret : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_api_secret')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label class="form-label" for="basic-addon-name">Zoom Link</label>
										<input type="text" id="zoom_url" name="zoom_url" class="form-control" aria-label="Name" value="{{ $data ? $data->zoom_url : '' }}" aria-describedby="basic-addon-name" required />
										@error('zoom_url')
										<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>
							</div>`);
		}
	});
</script>
@endsection