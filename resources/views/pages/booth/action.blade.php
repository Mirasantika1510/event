@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="/app-assets/css/uploader.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Notifications</a>
						</li>
						<li class="breadcrumb-item active">
                            @if ($data !== null)
                                Update Notifications
                            @else
                                Create Notifications
                            @endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        @if ($data !== null)
		    <form action="{{ route('admin.booth.update',$data->id) }}" method="POST" enctype='multipart/form-data'>        
        @else
		    <form action="{{ route('admin.booth.store') }}" id="jquery-val-form" method="POST" enctype='multipart/form-data'>
        @endif
            @csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Booth Category</h4>
						</div>
						<div class="card-body">
                            <div class="file-upload  @error('icons') border-danger @enderror">
                                <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Icons</button>
                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" name="icons" />
                                    <div class="drag-text">
                                        @if ($data !== null)
                                            <img class="file-upload-image" src="/img/icons/{{ $data ? $data->icons : '' }}" alt="your image" />
                                        @else
                                            <h3>Drag and drop a file or select add Icons</h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="/img/icons/{{ $data ? $data->icons : '' }}" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Icons</span></button>
                                    </div>
                                </div>
                            </div>
                            @error('icons')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror

							<div class="form-group">
								<label class="form-label" for="name">Name</label>
                                <input type="text" id="name" class="form-control @error('name') border-danger @enderror" placeholder="Input Icons Name ..." value="{{ $data ? $data->name : '' }}" name="name" />
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                            </div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<script src="/app-assets/vendors/js/extensions/dropzone.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/form-file-uploader.js" defer></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $(function (){
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });

        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });
    });
</script>

@endsection