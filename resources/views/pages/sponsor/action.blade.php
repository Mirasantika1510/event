@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="/app-assets/css/uploader.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Sponsor</a>
						</li>
						<li class="breadcrumb-item active">
                            @if ($data !== null)
                                Update Sponsor
                            @else
                                Create Sponsor
                            @endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        @if ($data !== null)
		    <form action="{{ route('admin.sponsor.update',$data->id) }}" method="POST" enctype='multipart/form-data'>        
        @else
		    <form action="{{ route('admin.sponsor.store') }}" id="jquery-val-form" method="POST" enctype='multipart/form-data'>
        @endif
            @csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Sponsor</h4>
						</div>
						<div class="card-body">
                            <div class="form-group">
                                <label class="form-label" for="Status">Category</label>
                                <select class="form-control @error('category') border-danger @enderror" name="category">
                                    @if ($data !== null)
                                        <option value="{{ $data->category }}">{{ $data->category == 1 ? 'Gold' : 'Silver'}}{{ $data->category == 3 ? 'Bronze' : ''}}</option>
                                    @else
                                        <option value="0">Choise Type Sponsor</option>
                                        <option value="1">Gold</option>
                                        <option value="2">Silver</option>
                                        <option value="3">Bronze</option>
                                    @endif
                                </select>
                                @error('category')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="file-upload @error('image') border-danger @enderror">
                                <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add image</button>
                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" name="image" />
                                    <div class="drag-text">
                                        @if ($data !== null)
                                            <img class="file-upload-image" src="/img/sponsor/{{ $data ? $data->image : '' }}" alt="your image" />
                                        @else
                                            <h3>Drag and drop a file or select add Image</h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="/img/sponsor/{{ $data ? $data->image : '' }}" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                    </div>
                                </div>
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
								<label class="form-label" for="name">Url</label>
                                <input type="text" id="link" class="form-control @error('link') border-danger @enderror" placeholder="Input banner link ..." value="{{ $data ? $data->link : '' }}" name="link" />
                                @error('link')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            @if ($sort !== null)
                                <div class="form-group">
                                    <label class="form-label" for="name">Sort</label>
                                    <select name="sort" class="form-control">
                                        <option value="{{ $data->sort }}">{{ $data->sort }}</option>
                                        @foreach ($sort as $item)
                                            <option value="{{ $item->sort }}">{{ $item->sort }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<script src="/app-assets/vendors/js/extensions/dropzone.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/form-file-uploader.js" defer></script>

@if ($data !== null)
    @if ($data->status == '1')
        <script>
                $("#content-image").css({"display": "inline"});
                $('#url-youtube').hide();
        </script>
    @else
        <script>
            $("#content-image").hide();
            $('#url-youtube').css({"display": "inline"});
        </script>
    @endif
@endif

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $(function (){

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });

        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });

        $("select.status").change(function(){
            var selected = $(this).children("option:selected").val();
            if(selected == 1){
                $("#content-image").css({"display": "inline"});
                $('#url-youtube').hide();
            }else if(selected == 2){
                $("#content-image").hide();
                $('#url-youtube').css({"display": "inline"});
            }
        });
    });
</script>

@endsection