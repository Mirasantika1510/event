@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-pickadate.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.schedule.main_stage.index') }}">Polling</a>
						</li>
						<li class="breadcrumb-item active">
							@if ($data !== null)
							Update Polling
							@else
							Create Polling
							@endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		@if ($data !== null)
		<form action="{{ route('admin.polling.update', $data->id) }}" method="POST" enctype="multipart/form-data">        
		@else
		<form action="{{ route('admin.polling.store') }}" id="jquery-val-form" method="POST" enctype="multipart/form-data">
		@endif
		@csrf
		<div class="row">
			<!-- Bootstrap Validation -->
			<div class="col-md-12 col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Polling</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Name</label>

							<input type="text" id="name" name="name" class="form-control" aria-label="Name" value="{{ $data ? $data->name : '' }}" aria-describedby="basic-addon-name" required />
							@error('name')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-12">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Question</label>

							<input type="text" id="question" name="question" class="form-control" aria-label="Name" value="{{ old('question') }}" aria-describedby="basic-addon-name" />
							@error('question')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="row">
							<div class="col-md-10">
								<div class="form-group">
									<label class="form-label" for="basic-addon-name">Option (At least 2 options)</label>

									<input type="text" id="option" name="option" class="form-control " aria-label="Name" value=" " aria-describedby="basic-addon-name" />
								</div>
							</div>
							<div class="col-md-2">
								<label class="form-label" for="basic-addon-name"></label>
								<div id="button-option">
									<button class="btn btn-info add-edit" type="button" id="add-option"><span class="fa fa-plus"></span></button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Option List</label>

							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>Option</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody id="tbody">
										
									</tbody>
								</table>
							</div>
						</div>
						<div class="row mt-2">
							<div class="col-md-6">
								<button type="button" class="btn btn-danger" id="add-list">Add List</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-12">
				<h4 class="card-title text-center">Polling Question List</h4>
				<div id="list-question">
					@if($pollings)
					@foreach($pollings as $item)
					<div class="card" id="card-{{ $item->id }}">
						<div class="card-body">
							<div class="row">
								<div class="col-md-10">
									{{ $item->question }}
								</div>
								<div class="col-md-2">
									<button type="button" data-id="{{ $item->id }}" class="btn btn-danger delete-question"><i class="fa fa-trash"></i></button>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif
				</div>
			</div>
		</div>

		<div id="input-option">
			@php $no = 1 @endphp
			@if($pollings)
			@foreach($pollings as $item)
			@php $no += $item->id @endphp
			<input type="hidden" name="question[]" id="question-{{ $item->id }}" value="{{ $item->question }}">
				<input type="hidden" name="option[]" id="option-{{ $item->id }}"  value="{{ $item->options }}">
			@endforeach
			@endif
		</div>

		<div class="row mb-4">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</section>
</div>
<input type="hidden" id="number" value="{{ $no }}" >
@endsection

@section('script')
<script type="text/javascript">
	$(document).on('click', '#add-option', function () {
		var option 		= $('#option').val();
		var question 	= $('#question').val();
		var name 		= $('#name').val();
		var number 		= $('#number').val();
		if (option 		=== ' ') {
			$('#option').addClass('is-invalid');
		} else {
			$('#option').removeClass('is-invalid');
		}
		if (question == '') {
			$('#question').addClass('is-invalid');
		} else {
			$('#question').removeClass('is-invalid');
		}
		if (name == '') {
			$('#name').addClass('is-invalid');
		} else {
			$('#name').removeClass('is-invalid');
		}
		if (option !== ' ' && question !== '' && name !== '') {
		$('#number').val(parseInt(number) + parseInt(1));
		$('#tbody').append(`
			<tr id="tr-${number}">
				<td id="td-${number}" class="td-option">${option}</td>
				<td><button class="btn btn-info edit" data-value="${option}" data-id="${number}" type="button"><span class="fa fa-edit"></span></button> <button class="btn btn-danger delete" data-id="${number}" type="button"><span class="fa fa-trash"></span></button></td>
			</tr>
			`);
		$('#option').val(' ')
		}
	});
	$(document).on('click', '.edit', function () {
		var id = $(this).attr('data-id');
		var value = $('#td-'+id).html();
		$('#option').val(value);

		$('#button-option').html(`<button class="btn btn-success add-edit" data-id="${id}" type="button" id="edit-option"><span class="fa fa-check"></span></button>`);
	});

	$(document).on('click', '.delete', function () {
		var id = $(this).attr('data-id');
		$('#tr-'+id).remove();
	});
	$(document).on('click', '.delete-question', function () {
		var id = $(this).attr('data-id');

		$('#card-'+id).remove();
		$('#question-'+id).remove();
		$('#option-'+id).remove();
	});

	$(document).on('click', '#edit-option', function () {
		var option = $('#option').val();
		var id = $(this).attr('data-id');
		if (option === ' ') {
			$('#option').addClass('is-invalid');
		} else {
			$('#option').removeClass('is-invalid');
		}
		if (option !== ' ') {
		$('#td-'+id).html(option);
		$('#option').val(' ')
		$('#button-option').html(`<button class="btn btn-info add-edit" type="button" id="add-option"><span class="fa fa-plus"></span></button>`);
		}
	});

	$(document).on('click', '#add-list', function () {
		var option 		= $('#option').val();
		var question 	= $('#question').val();
		var name 		= $('#name').val();
		var td 			= $(".td-option").length;
		var result 		= '';
		for (var i = 0; i < td; i++) {
			result += document.getElementsByClassName('td-option')[i].innerHTML + '|';
		}

		if (question == '') {
			$('#question').addClass('is-invalid');
		} else {
			$('#question').removeClass('is-invalid');
		}
		if (name == '') {
			$('#name').addClass('is-invalid');
		} else {
			$('#name').removeClass('is-invalid');
		}
		if (question !== '' && name !== '' && td >= 2) {
			var number 		= $('#number').val();
			$('#input-option').append(`
				<input type="hidden" name="question[]" id="question-${number}" value="${question}">
				<input type="hidden" name="option[]" id="option-${number}"  value="${result}">
				`);
			$('#option').val('');
			$('#question').val('');
			$('#tbody').html('');
			$('#list-question').append(`
								<div class="card" id="card-${number}">
									<div class="card-body">
										<div class="row">
											<div class="col-md-10">
												${question}
											</div>
											<div class="col-md-2">
												<button type="button" data-id="${number}" class="btn btn-danger delete-question"><i class="fa fa-trash"></i></button>
											</div>
										</div>
									</div>
								</div>
				`);
		}
	})
</script>
@endsection