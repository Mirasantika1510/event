@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-pickadate.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Notifications</a>
						</li>
						<li class="breadcrumb-item active">
                            @if ($data !== null)
                                Update Notifications
                            @else
                                Create Notifications
                            @endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        @if ($data !== null)
		    <form action="{{ route('admin.notif.update',$data->id) }}" method="POST">        
        @else
		    <form action="{{ route('admin.notif.store') }}" id="jquery-val-form" method="POST">
        @endif
            @csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Notifications</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Message</label>

								<textarea name="message" id="message" class="form-control @error('message') border-danger @enderror" cols="30" rows="10">{{ $data ? $data->message : '' }}</textarea>
                                @error('message')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
							</div>
							<div class="form-group">
								<label class="form-label" for="fp-date-time">Send At</label>
                                <input type="text" id="fp-date-time" class="form-control flatpickr-date-time @error('send_at') border-danger @enderror" placeholder="YYYY-MM-DD HH:MM" value="{{ $data ? $data->send_at : '' }}" name="send_at" />
                                @error('send_at')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>

<script src="/app-assets/vendors/js/pickers/pickadate/picker.js" defer></script>
<script src="/app-assets/vendors/js/pickers/pickadate/picker.date.js" defer></script>
<script src="/app-assets/vendors/js/pickers/pickadate/picker.time.js" defer></script>
<script src="/app-assets/vendors/js/pickers/pickadate/legacy.js" defer></script>
<script src="/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/pickers/form-pickers.js" defer></script>

@endsection