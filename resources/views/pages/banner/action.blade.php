@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="/app-assets/css/uploader.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Banner</a>
						</li>
						<li class="breadcrumb-item active">
                            @if ($data !== null)
                                Update Banner
                            @else
                                Create Banner
                            @endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
        @if ($data !== null)
		    <form action="{{ route('admin.banner.update',$data->id) }}" method="POST" enctype='multipart/form-data'>        
        @else
		    <form action="{{ route('admin.banner.store') }}" id="jquery-val-form" method="POST" enctype='multipart/form-data'>
        @endif
            @csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Banner</h4>
						</div>
						<div class="card-body">
                            <div class="form-group">
                                <label class="form-label" for="Status">Status</label>
                                <select class="form-control status @error('status') border-danger @enderror" name="status">
                                    @if ($data !== null)
                                        <option value="{{ $data->status }}">{{ $data->status == 1 ? 'Image' : 'Youtube'}}</option>
                                    @else
                                        <option value="0">Choise Type Banner</option>
                                        <option value="1">Image</option>
                                        <option value="2">Youtube</option>
                                    @endif
                                </select>
                                @error('status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="file-upload @error('image') border-danger @enderror" id="content-image" style="display: none;">
                                <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add image</button>
                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" name="image" />
                                    <div class="drag-text">
                                        @if ($data !== null)
                                            <img class="file-upload-image" src="/img/banner/{{ $data ? $data->image : '' }}" alt="your image" />
                                        @else
                                            <h3>Drag and drop a file or select add Image</h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="/img/banner/{{ $data ? $data->image : '' }}" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                    </div>
                                </div>
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            
                            <div class="form-group" id="url-youtube" style="display: none;">
								<label class="form-label" for="name">Url</label>
                                <input type="text" id="url" class="form-control @error('url') border-danger @enderror" placeholder="Input banner url ..." value="{{ $data ? $data->url : '' }}" name="url" />
                                @error('url')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            @if ($order !== null)
                                <div class="form-group">
                                    <label class="form-label" for="name">Sort</label>
                                    <select name="order" class="form-control">
                                        <option value="{{ $data->order }}">{{ $data->order }}</option>
                                        @foreach ($order as $item)
                                            <option value="{{ $item->order }}">{{ $item->order }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<script src="/app-assets/vendors/js/extensions/dropzone.min.js" defer></script>
<script src="/app-assets/js/scripts/forms/form-file-uploader.js" defer></script>

@if ($data !== null)
    @if ($data->status == '1')
        <script>
                $("#content-image").css({"display": "inline"});
                $('#url-youtube').hide();
        </script>
    @else
        <script>
            $("#content-image").hide();
            $('#url-youtube').css({"display": "inline"});
        </script>
    @endif
@endif

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $(function (){

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });

        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });

        $("select.status").change(function(){
            var selected = $(this).children("option:selected").val();
            if(selected == 1){
                $("#content-image").css({"display": "inline"});
                $('#url-youtube').hide();
            }else if(selected == 2){
                $("#content-image").hide();
                $('#url-youtube').css({"display": "inline"});
            }
        });
    });
</script>

@endsection