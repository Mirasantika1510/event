@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Event
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.event-update', $event->id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Event</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Event Name</label>

								<input type="text" id="basic-addon-name" name="event_name" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $event ? $event->action['event_name'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Date Start</label>

								<input type="date" id="basic-addon-name" name="date_start" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $event ? $event->action['date_start'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Date End</label>

								<input type="date" id="basic-addon-name" name="date_end" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $event ? $event->action['date_end'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label for="select-country1">Timezone </label>
								<select class="form-control" id="select-country1" name="timezone" required>
									<option value="{{ $event ? $event->action['timezone'] : '' }}">{{ $event ? $event->action['timezone'] : 'Select Timezone' }}</option>
									<option value="Asia/Jakart">Asia/Jakarta</option>
									<option value="Asia/Makassar">Asia/Makassar</option>
									<option value="Asia/Jayapura">Asia/Jayapura</option>
								</select>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please select your country</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Logo & Icon</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label for="customFile1">Logo</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="logo" onchange="loadFile(event)" id="customFile1" />
									<label class="custom-file-label" for="customFile1">Choose logo pic</label>
								</div>
								<small>* If you want to upload other file, please first delete the file.</small>
								<img
                                    id="foto_profile"
                                    class="m-2"
                                    style="width:20%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;"
                                    src="/img/event/{{ $event->action['logo'] }}">
							</div>
							<div class="form-group">
								<label for="customFile1">Icon</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="icon" onchange="loadFile2(event)" id="customFile1" />
									<label class="custom-file-label" for="customFile1">Choose icon pic</label>
								</div>
								<small>* If you want to upload other file, please first delete the file.</small>
								<img
                                    id="foto_profile2"
                                    class="m-2"
                                    style="width:20%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;"
                                    src="/img/event/{{ $event->action['icon'] }}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Color</h4>
						</div>
						<div class="card-body">

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Primary Color</label>

								<input type="color" value="{{ $event ? $event->action['primary_color'] : '' }}" id="basic-addon-name" name="primary_color" class="form-control" placeholder="Primary Color" aria-label="Name" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your primary color.</div>
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Secondary Color</label>

								<input type="color" value="{{ $event ? $event->action['secondary_color'] : '' }}" id="basic-addon-name" name="secondary_color" class="form-control" placeholder="Secondary Color" aria-label="Name" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your secondary color.</div>
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Tertiary Color</label>

								<input type="color" value="{{ $event ? $event->action['tertiary_color'] : '' }}" id="basic-addon-name" name="tertiary_color" class="form-control" placeholder="Tertiary Color" aria-label="Name" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your primary color.</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

	});
</script>
@endif
@endsection