@extends('layouts.app')
@section('content')
<section id="dashboard-analytics">
	<div class="row match-height">

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Event</h2>
							<p>General settings such as, event name, event date, menu etc.</p>
							<a href="{{ route('admin.preferences.event') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Zoom</h2>
							<p>General settings such as, zoom api key and zoom api secret.</p>
							<a href="{{ route('admin.preferences.zoom') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Landing Page</h2>
							<p>Landing Page settings, such as poster image, about, speakers etc.</p>
							<a href="{{ route('admin.preferences.landing') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Login & Register</h2>
							<p>Login & Register Settings such as title, banner, caption etc.</p>
							<a href="{{ route('admin.preferences.login') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Payment</h2>
							<p>Payment Settings, such as title, caption, contact, bank transfer etc.</p>
							<a href="{{ route('admin.preferences.payment') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

		<!-- Subscribers Chart Card starts -->
		<div class="col-lg-6 col-sm-6 col-12">
			<div class="card">
				<div class="row">
					<div class="col-lg-3">
						
						<div class="card-header flex-column align-items-start pb-0">
							<div class="avatar bg-light-primary p-50 m-0">
								<div class="avatar-content">
									<i data-feather="users" class="font-medium-5"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9">
						
						<div class="card-header flex-column align-items-start pb-0">
							<h2 class="font-weight-bolder mt-1">Web</h2>
							<p>Web Settings, such as menu icon, status, design etc.</p>
							<a href="{{ route('admin.preferences.web') }}" class="my-2">Change Setting <i data-feather="chevron-right" class="font-medium-5"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Subscribers Chart Card ends -->

	</div>
</section>
@endsection