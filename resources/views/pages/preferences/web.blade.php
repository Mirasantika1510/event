@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Event
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.web-update', $event->id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Home</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Home Menu Text </label>

								<input type="text" id="basic-addon-name" name="home_menu_text" class="form-control"aria-label="Name" value="{{ $event ? $event->action['home_menu_text'] : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Home Menu Icon Active </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="home_menu_icon_active" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['home_menu_icon_active'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Home Menu Icon Nonactive </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="home_menu_icon_nonactive" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['home_menu_icon_nonactive'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Booth</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Booth Menu Text </label>

								<input type="text" id="basic-addon-name" name="booth_menu_text" class="form-control"aria-label="Name" value="{{ $event ? $event->action['booth_menu_text'] : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Booth Menu Icon Active </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="booth_menu_icon_active" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['booth_menu_icon_active'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Booth Menu Icon Nonactive </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="booth_menu_icon_nonactive" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['booth_menu_icon_nonactive'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Main Stage</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Main Stage Menu Text </label>

								<input type="text" id="basic-addon-name" name="main_stage_menu_text" class="form-control"aria-label="Name" value="{{ $event ? $event->action['main_stage_menu_text'] : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Main Stage Menu Icon Active </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="main_stage_menu_icon_active" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['main_stage_menu_icon_active'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Main Stage Menu Icon Nonactive </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="main_stage_menu_icon_nonactive" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['main_stage_menu_icon_nonactive'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Classroom</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Classroom Menu Text </label>

								<input type="text" id="basic-addon-name" name="classroom_menu_text" class="form-control"aria-label="Name" value="{{ $event ? $event->action['classroom_menu_text'] : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Classroom Menu Icon Active </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="classroom_menu_icon_active" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['classroom_menu_icon_active'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Classroom Menu Icon Nonactive </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="classroom_menu_icon_nonactive" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['classroom_menu_icon_nonactive'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Sub Menu</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Schedule Icon </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="schedule_icon" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['schedule_icon'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Sponsor Icon </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="sponsor_icon" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['sponsor_icon'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Booth Floor Plan Icon </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="booth_plan_icon" onchange="loadFile(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['booth_plan_icon'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Booth List icon </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="booth_plan_icon" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['booth_plan_icon'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-5 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Booth</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<div class="mb-2">
									<audio controls>
										<source src="horse.ogg" type="audio/ogg">
										<source src="horse.mp3" type="audio/mpeg">
									</audio>
								</div>
								<label for="customFile1">Booth Audio </label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="booth_audio" onchange="loadFile(event)" id="customFile1" />
									<label class="custom-file-label" for="customFile1">Choose Image</label>
								</div>
								
							</div>
							<div class="form-group">
								<label for="customFile1">Booth Side Banner </label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="booth_banner" onchange="loadFile2(event)" id="customFile1" />
									<label class="custom-file-label" for="customFile1">Choose Image</label>
								</div>
								<img
								id="foto_profile3"
								class="m-2"
								style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
								src="/img/landing/{{ $event->action['booth_banner'] ? : 'default.jpg' }}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Main Menu</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-4">
									<div class="demo-inline-spacing">
										<div class="custom-control custom-control-primary custom-switch">
											<div><small class="mb-50" style="font-size: 10px;">Booth Menu Status </small></div>
											<input type="checkbox" checked name="booth_menu_status" class="custom-control-input" id="customSwitch3" />
											<label class="custom-control-label" for="customSwitch3"></label>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="demo-inline-spacing">
										<div class="custom-control custom-control-primary custom-switch">
											<div><small class="mb-50" style="font-size: 10px;">Main Stage Menu Status</small></div>
											<input type="checkbox" checked name="main_stage_menu_status" class="custom-control-input" id="customSwitch3" />
											<label class="custom-control-label" for="customSwitch3"></label>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="demo-inline-spacing">
										<div class="custom-control custom-control-primary custom-switch">
											<div><small class="mb-50" style="font-size: 10px;">Classroom Menu Status</small></div>
											<input type="checkbox" checked name="classroom_menu_status" class="custom-control-input" id="customSwitch3" />
											<label class="custom-control-label" for="customSwitch3"></label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Homepage</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Home Running Text </label>

								<input type="text" id="basic-addon-name" name="home_running_text" class="form-control"aria-label="Name" value="{{ $event ? $event->action['home_running_text'] : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="form-group">
								<label for="select-country1">Home Slider Type </label>
								<select class="form-control" id="select-country1" name="home_slider_type" required>
									<option value="{{ $event ? $event->action['home_slider_type'] : 'Select Home Slider Type' }}">{{ $event ? $event->action['home_slider_type'] : 'Select Home Slider Type' }}</option>
									<option value="image">Image</option>
									<option value="youtube">Youtube</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Design</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Home Background Image </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="home_background_image" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['home_background_image'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Booth Background Image </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="both_background_image" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['both_background_image'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Main Stage Background Image </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="main_stage_background_image" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['main_stage_background_image'] ? : 'default.jpg' }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="customFile1">Classroom Background Image </label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="classroom_background_image" onchange="loadFile2(event)" id="customFile1" />
											<label class="custom-file-label" for="customFile1">Choose Image</label>
										</div>
										<img
										id="foto_profile3"
										class="m-2"
										style="width:80%;border: 1px solid #ddd;border-radius: 4px;padding: 2px;"
										src="/img/landing/{{ $event->action['classroom_background_image'] ? : 'default.jpg' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

		});
	</script>
	@endif
	@endsection