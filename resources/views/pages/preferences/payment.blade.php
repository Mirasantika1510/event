@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Event
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.payment-update', $event->id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Ticket</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Ticket Title Text </label>

								<input type="text" id="basic-addon-name" name="ticket_title" class="form-control"aria-label="Name" value="{{ $event ? $event->action['ticket_title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Ticket Caption Text </label>

								<input type="text" id="basic-addon-name" name="ticket_caption" class="form-control"aria-label="Name" value="{{ $event ? $event->action['ticket_caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Classroom</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Classroom Title Text </label>

								<input type="text" id="basic-addon-name" name="classroom_title" class="form-control"aria-label="Name" value="{{ $event ? $event->action['classroom_title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Classroom Caption Text </label>

								<input type="text" id="basic-addon-name" name="classroom_caption" class="form-control"aria-label="Name" value="{{ $event ? $event->action['classroom_caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Order Summary</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Order Summary Title Text </label>

								<input type="text" id="basic-addon-name" name="order_summary_title" class="form-control"aria-label="Name" value="{{ $event ? $event->action['order_summary_title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Order Summary Caption Text </label>

								<input type="text" id="basic-addon-name" name="order_summary_caption" class="form-control"aria-label="Name" value="{{ $event ? $event->action['order_summary_caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Payment</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Title Text </label>

								<input type="text" id="basic-addon-name" name="payment_title" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Caption Text </label>

								<input type="text" id="basic-addon-name" name="payment_caption" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Contact Settings</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Contact Email Events  </label>

								<input type="text" id="basic-addon-name" name="contact_email" class="form-control"aria-label="Name" value="{{ $event ? $event->action['contact_email'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Contact Name 1 </label>

								<input type="text" id="basic-addon-name" name="contact_name1" class="form-control"aria-label="Name" value="{{ $event ? $event->action['contact_name1'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Contact Phone 1 </label>

								<input type="text" id="basic-addon-name" name="contact_phone1" class="form-control"aria-label="Name" value="{{ $event ? $event->action['contact_phone1'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Contact Name 2 (Optional)</label>

								<input type="text" id="basic-addon-name" name="contact_name2" class="form-control"aria-label="Name" value="{{ $event ? $event->action['contact_name2'] : '' }}" aria-describedby="basic-addon-name" />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Contact Phone 2 (Optional)</label>

								<input type="text" id="basic-addon-name" name="contact_phone2" class="form-control"aria-label="Name" value="{{ $event ? $event->action['contact_phone2'] : '' }}" aria-describedby="basic-addon-name" />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Bank Transfer</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Bank Name</label>

								<input type="text" id="basic-addon-name" name="payment_bank_name" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_bank_name'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Bank Branch</label>

								<input type="text" id="basic-addon-name" name="payment_bank_branch" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_bank_branch'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Account Owner</label>

								<input type="text" id="basic-addon-name" name="payment_account_owner" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_account_owner'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Account Number </label>

								<input type="text" id="basic-addon-name" name="payment_account_number" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_account_number'] : '' }}" aria-describedby="basic-addon-name" required="" />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>


				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Payment Confirmation</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Confirmation Title Text </label>

								<input type="text" id="basic-addon-name" name="payment_confirmation_title" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_confirmation_title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Payment Confirmation Caption Text </label>

								<input type="text" id="basic-addon-name" name="payment_confirmation_caption" class="form-control"aria-label="Name" value="{{ $event ? $event->action['payment_confirmation_caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

	});
</script>
@endif
@endsection