@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Login
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.login-update', $data->id) }}" method="POST"  enctype="multipart/form-data">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Login</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Login Title Text </label>

								<input type="text" id="basic-addon-name" name="title" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $data ? $data->action['title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Login Caption Text </label>

								<input type="text" id="basic-addon-name" name="caption" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $data ? $data->action['caption'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Register</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Register Title Text </label>

								<input type="text" id="basic-addon-name" name="title_register" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $data ? $data->action['title_register'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Register Caption Text </label>

								<input type="text" id="basic-addon-name" name="caption_register" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $data ? $data->action['caption_register'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label for="customFile1">Login Image *</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="logo" onchange="loadFile(event)" id="customFile1" />
									<label class="custom-file-label" for="customFile1">Choose logo pic</label>
								</div>
								<small>* If you want to upload other file, please first delete the file.</small>
								<img
                                    id="foto_profile"
                                    class="m-2"
                                    style="width:20%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;"
                                    src="/img/login/{{ $data->action['logo'] }}">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

	});
</script>
@endif
@endsection