@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Landing Page
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.landing-update', $landing->id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Landing Page Banner</label>

								<input type="file" id="basic-addon-name" name="image" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $landing ? $landing->action['image'] : '' }}" aria-describedby="basic-addon-name" onchange="loadFile(event)"/>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
								<small>* If you want to upload other file, please first delete the file.</small>
								<img
                                    id="foto_profile"
                                    class="m-2"
                                    style="width:20%;border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;"
                                    src="/img/landing/{{ $landing->action['image'] }}">
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Landingpage About Title Text *</label>

								<input type="text" id="basic-addon-name" name="title" class="form-control" aria-label="Name" value="{{ $landing ? $landing->action['title'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Landingpage About Description Text *</label>
								<textarea name="description" class="form-control" required="">{{ $landing ? $landing->action['description'] : '' }}
								</textarea>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Landingpage Footer Text *</label>

								<input type="text" id="basic-addon-name" name="footer_text" class="form-control" aria-label="Name" value="{{ $landing ? $landing->action['footer_text'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 mx-auto d-block">
					<div class="row mb-4">
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary">Save Changes</button>
						</div>
					</div>
				</div>
			</form>

			<div class="col-md-6 col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Add New Speaker</h4>
					</div>
					<div class="card-body">
						<form action="{{ route('admin.preferences.speaker.create') }}" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Name</label>

								<input type="text" id="basic-addon-name" name="name" class="form-control" aria-label="Name" value="{{ old('name') }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>

							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Title (optional)</label>

								<input type="text" id="basic-addon-name" name="title" class="form-control" aria-label="Name" value="{{ old('title') }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>

							<div class="form-group">
								<label for="customFile1">Profile Photo (optional)</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="image" id="customFile1" required />
									<label class="custom-file-label" for="customFile1">Choose profile pic</label>
								</div>
								<small>* Max size 500KB Square dimentions</small>
							</div>
							<div class="form-group">
								<input type="submit" value="Add" class="btn btn-warning">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-12">
				<h4 class="card-title">Speakers</h4>
				@foreach($speaker as $item)
				<div class="media-list ">
					<div class="media bg-white">
						<div class="media-left">
							<img src="/img/speaker/{{ $item->image }}" alt="avatar" height="64" width="64" class="cursor-pointer" />
						</div>
						<div class="media-body">
							<h4 class="media-heading mt-2">{{ $item->name }} {{ $item->title ? '('.$item->title.')' : '' }}</h4>
						</div>
						<div class="media-right">
							<a href="{{ route('admin.preferences.speaker.delete', $item->id) }}" ><i data-feather="trash-2" class="font-medium-3"></i></a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

	});
</script>
@endif
@endsection