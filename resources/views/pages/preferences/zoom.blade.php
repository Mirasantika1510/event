@extends('layouts.app')
@section('content')

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('admin.preferences.index') }}">Preferences</a>
						</li>
						<li class="breadcrumb-item active">Zoom
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		<form action="{{ route('admin.preferences.zoom-update', $zoom->id) }}" method="POST">
			@csrf
			<div class="row">
				<!-- Bootstrap Validation -->
				<div class="col-md-12 col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Zoom</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Zoom API Key </label>

								<input type="text" id="basic-addon-name" name="zoom_api_key" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $zoom ? $zoom->action['zoom_api_key'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Zoom API Secret </label>

								<input type="text" id="basic-addon-name" name="zoom_api_secret" class="form-control" placeholder="Event Name" aria-label="Name" value="{{ $zoom ? $zoom->action['zoom_api_secret'] : '' }}" aria-describedby="basic-addon-name" required />
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mb-4">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>
		</form>
	</section>
</div>
@endsection
@section('script')
@if(Session::get('success'))
<script type="text/javascript">
	$(document).ready(function(){

		 // Success Type
		 toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
		 	closeButton: true,
		 	tapToDismiss: false,
		 	progressBar: true,
		 });

	});
</script>
@endif

@endsection