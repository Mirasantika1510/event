@extends('layouts.app')
@section('content')

<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Payment Confirmation</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a>
                            </li>
                            <li class="breadcrumb-item active"><a href="#">Payment Confirmation</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="card mt-2">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2"><img src="/app-assets/images/align-justify.svg"> Confirmation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>No Invoice</th>
                                <td>{{ $payment->order_id }}</td>
                            </tr>
                            <tr>
                                <th>Amount</th>
                                <td>Rp. {{ number_format($payment->amount) }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ strtoupper($payment->status) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Basic table -->
        <section id="ajax-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h4 class="card-title">Payment Confirmation</h4>
                        </div>
                        <div class="card-datatable">
                            <table class="table" id="data-notif">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Order Id</th>
                                        <th>Image Payment</th>
                                        <th>Acount Name</th>
                                        <th>Amount</th>
                                        <th>Transfer Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Basic table -->


    </div>
</div>

@endsection

@section('script')

<!-- BEGIN: Page Vendor JS-->
<script src="/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js" defer></script>

<script src="/app-assets/js/scripts/tables/table-datatables-advanced.js" defer></script>
<script src="/app-assets/js/scripts/components/components-dropdowns.js"></script>
<!-- END: Page Vendor JS-->
<!-- BEGIN: Page JS-->


<script>
    $(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#data-notif').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.payments.history.confirmation', $payment->id) }}",
            columns: [
            {
                data: 'no',
               "render" : function (data, type, row) {
                    return ' ';
                } 
            },
            {
                data: 'order_id',
                name: 'order_id',
            },
            {
                data: 'image',
                "render" : function (data, type, row) {
                    if(row.image == '0')
                    {
                        return '-';
                    }else {
                        return `<img src="/img/payments/${row.image}" width="100">`;
                    }
                    
                } 
            },
            {
                data: 'account_name',
                name: 'account_name',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
            {
                data: 'status',
               "render" : function (data, type, row) {
                    if(row.status == 'waiting')
                    {
                        return `<div class="btn-group">
                        <span class="badge badge-warning dropdown-toggle" id="dropdownMenuButton100" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        WAITING
                        </span>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/admin/payments/confirmation/approved/${row.id}">Appproved</a>
                        <a class="dropdown-item" href="/admin/payments/confirmation/rejected/${row.id}">Rejected</a>
                        </div>
                        </div>`;
                    }else if(row.status == 'approved'){                                
                        return '<span class="badge badge-success">APPROVED</span>';
                    } else {
                        return '<span class="badge badge-danger">REJECTTED</span>';
                    }
                    
                } 
            },
            ],
            columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0
            }
            ],
            dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            orderCellsTop: true,
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            drawCallback: () => {
                $('.delete').click(function () {
                    const id = $(this).data(id)
                })
            }
        });
    });

    function hapus(id){
        clearToastObj = toastr['error'](
            'Are You Delete?<br /><br /><button type="button" class="btn btn-danger btn-sm delete">Yes</button>',
            'Deleted',
            {
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0,
                tapToDismiss: false,
            }
            );

        if (clearToastObj.find('.delete').length) {
            clearToastObj.delegate('.delete', 'click', function () {
                toastr.clear(clearToastObj, { force: true });
                clearToastObj = undefined;
                $.ajax({
                    method: "GET",
                    url: "/admin/ticket" + '/' + id,
                    success: function (data) {
                        toastr['success']('👋 Yes Data Deleted !!.', 'Progress Bar', {
                            closeButton: true,
                            tapToDismiss: false,
                            progressBar: true,
                        });
                        location.reload();
                    },
                    error: function (data) {
                        toastr['success']('👋 Chocolate oat cake jelly oat cake candy jelly beans pastry.', 'Progress Bar', {
                            closeButton: true,
                            tapToDismiss: false,
                            progressBar: true,
                            rtl: isRtl
                        });
                    }
                });
            });
        }
    }
</script>
<script>
    $(function (){
        $('.hapus').on('click', function () {
            var id = $(this).attr('data-id');
            console.log(id);
        });
    });
</script>
@if(Session::get('success'))
<script type="text/javascript">
    $(document).ready(function(){

         // Success Type
         toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
            closeButton: true,
            tapToDismiss: false,
            progressBar: true,
        });

     });
 </script>
 @endif
 @endsection