@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-pickadate.css">

<div class="content-header row">
	<div class="content-header-left col-md-9 col-12 mb-2">
		<div class="row breadcrumbs-top">
			<div class="col-12">
				<div class="breadcrumb-wrapper">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Ticket</a>
						</li>
						<li class="breadcrumb-item active">
							@if ($data !== null)
							Update Ticket
							@else
							Create Ticket
							@endif
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<!-- Validation -->
	<section class="bs-validation">
		@if ($data !== null)
		<form action="{{ route('admin.ticket.update', $data->id) }}" method="POST">        
		@else
		<form action="{{ route('admin.ticket.store') }}" id="jquery-val-form" method="POST">
		@endif
		@csrf
		<div class="row">
			<!-- Bootstrap Validation -->
			<div class="col-md-12 col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Ticket</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Name</label>

							<input type="text" id="basic-addon-name" name="name" class="form-control" aria-label="Name" value="{{ $data ? $data->name : '' }}" aria-describedby="basic-addon-name" required />
						</div>
						<div class="form-group">
							<label class="form-label" for="basic-addon-name">Description</label>

							<textarea name="description" id="message" class="form-control @error('description') border-danger @enderror" cols="30" rows="10">{{ $data ? $data->description : '' }}</textarea>
							@error('description')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<label for="select-country1">Type </label>
							<select class="form-control" id="select-country1" name="type" required>
								@if($data)
								@if($data->type == 'paid')
								<option value="paid">Paid</option>
								<option value="free">Free</option>
								@else
								<option value="free">Free</option>
								<option value="paid">Paid</option>
								@endif
								@else
								<option value="paid">Paid</option>
								<option value="free">Free</option>
								@endif
							</select>
						</div>
						<div id="type-paid">
							@if($data)
							@if($data->type == 'paid')
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Price</label>

								<input type="number" id="basic-addon-name" name="price" class="form-control" aria-label="Name" value="{{ $data ? $data->price : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="form-group">
								<label class="d-block">Ticket Only ?</label>
								<div class="custom-control custom-radio my-50">
									<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->ticket_only == 1 ? 'checked' : '' : '' }} />
									<label class="custom-control-label" for="validationRadio3">Yes</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->ticket_only == 0 ? 'checked' : '' : '' }}/>
									<label class="custom-control-label" for="validationRadio4">No</label>
								</div>
							</div>
							@else
							@endif
							@else
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Price</label>

								<input type="number" id="basic-addon-name" name="price" class="form-control" aria-label="Name" value="{{ $data ? $data->price : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="form-group">
								<label class="d-block">Ticket Only ?</label>
								<div class="custom-control custom-radio my-50">
									<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->ticket_only == 1 ? 'checked' : '' : 'checked' }} />
									<label class="custom-control-label" for="validationRadio3">Yes</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->ticket_only == 0 ? 'checked' : '' : '' }}/>
									<label class="custom-control-label" for="validationRadio4">No</label>
								</div>
							</div>
							@endif
							
						</div>
					</div>
				</div>
				<div id="ticket">
					@if($data)
					@if($data->ticket_only == '0')
					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-body">
									<div class="form-group">
										<label class="form-label" for="basic-addon-name">Classroom Schedule</label>

										<select class="form-control" id="classroom_schedule" name="classroom_schedule" required>
											<option disabled="" selected="">Please Select a Classroom Schedule</option>
											@foreach($classroom as $item)
											<option value="{{ $item->id }}" data-title="{{ $item->title }}">{{ $item->title }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label class="form-label" for="basic-addon-name">Price</label>

										<input type="number" id="price_schedule" name="price_schedule" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
									</div>
									 <button class="btn btn-primary" type="button" id="add-schedule">Add</button>

								</div>
							</div>
						</div>
						<div class="col-md-6" id="schedule">
							<h4>Ticket Schedule List</h4>
							<br>
							@foreach($ticketList as $item)
							<div class="card">
								<div class="card-body">
									<div class="row">
										<div class="col-md-10">
											{{ $item->schedule->title }} (Rp. {{ $item->price }})
										</div>
										<input type="hidden" value="{{ $item->schedule_id }}" name="schedule_id[]">
										<input type="hidden" value="{{ $item->price }}" name="schedule_price[]">
										<div class="col-md-2">
											<!-- <a ><i data-feather="edit" class="font-medium-3"></i></a> -->
											<a href="{{ route('admin.ticket.schedule.delete', $item->id) }}"><i data-feather="trash-2" class="font-medium-3"></i></a>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
					@endif
					@endif
				</div>
			</div>
		</div>

		<div class="row mb-4">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</div>
	</form>
</section>
</div>
<input type="hidden" id="schedule_classroom">

@endsection

@section('script')
<script type="text/javascript">
	$(document).on('click', '#validationRadio3', function () {
		$('#ticket').html(``);
	});
	$(document).on('click', '#validationRadio4', function () {
		$('#ticket').html(`<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-body">
									<div class="form-group">
										<label class="form-label" for="basic-addon-name">Classroom Schedule</label>

										<select class="form-control" id="classroom_schedule" name="classroom_schedule" required>
											<option disabled="" selected="">Please Select a Classroom Schedule</option>
											@foreach($classroom as $item)
											<option value="{{ $item->id }}" data-title="{{ $item->title }}">{{ $item->title }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label class="form-label" for="basic-addon-name">Price</label>

										<input type="number" id="price_schedule" name="price_schedule" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
									</div>
									 <button class="btn btn-primary" type="button" id="add-schedule">Add</button>

								</div>
							</div>
						</div>
						<div class="col-md-6" id="schedule">
							<h4>Ticket Schedule List</h4>
							<br>
						</div>
					</div>`);
	});
	$('#select-country1').on('change', function () {
		var type = $('#select-country1').val();
		if (type == 'paid') {
			$('#type-paid').html(`
							<div class="form-group">
								<label class="form-label" for="basic-addon-name">Price</label>

								<input type="number" id="basic-addon-name" name="price" class="form-control" aria-label="Name" value="{{ $data ? $data->price : '' }}" aria-describedby="basic-addon-name" required />
							</div>
							<div class="form-group">
								<label class="d-block">Ticket Only ?</label>
								<div class="custom-control custom-radio my-50">
									<input type="radio" id="validationRadio3" name="validationRadioBootstrap" class="custom-control-input" value="1" {{ $data ? $data->ticket_only == 1 ? 'checked' : '' : '' }} />
									<label class="custom-control-label" for="validationRadio3">Yes</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="validationRadio4" name="validationRadioBootstrap" class="custom-control-input" value="0" {{ $data ? $data->ticket_only == 0 ? 'checked' : '' : '' }}/>
									<label class="custom-control-label" for="validationRadio4">No</label>
								</div>
							</div>`);
		} else {
			$('#type-paid').html(``);
		}
	});

	$(document).on('change', '#classroom_schedule', function () {
		var schedule 	= $('#classroom_schedule').val();
		$.ajax ({
			method: "GET",
			url: "/admin/ticket/schedule/"+schedule, 
			data: {"_token": "{{ csrf_token() }}",}
		}).done( function(data){
			$('#schedule_classroom').val(data.title)
		}).fail(function(jqXHR, textStatus, error){
		});
	});

	$(document).on('click', '#add-schedule', function () {
		var schedule 	= $('#schedule_classroom').val();
		var id 			= $('#classroom_schedule').val();
		var price 		= $('#price_schedule').val();
		if (schedule == null) {
			$('#classroom_schedule').addClass('is-invalid');
		} else {
			$('#classroom_schedule').removeClass('is-invalid');
		}
		if (price == '') {
			$('#price_schedule').addClass('is-invalid');
		} else {
			$('#price_schedule').removeClass('is-invalid');
		}
		if (schedule != null && price != '') {
			$('#schedule').append(`<div class="card">
									<div class="card-body">
										<div class="row">
											<div class="col-md-10">
												`+schedule+`(Rp.`+price+`)
											</div>
											<input type="hidden" value="`+id+`" name="schedule_id[]">
											<input type="hidden" value="`+price+`" name="schedule_price[]">
											<div class="col-md-2">
												<a ><i data-feather="edit" class="font-medium-3"></i></a>
												<a ><i data-feather="trash-2" class="font-medium-3"></i></a>
											</div>
										</div>
									</div>
								</div>`);
			$('#classroom_schedule').html(`
											<option disabled="" selected="">Please Select a Classroom Schedule</option>
											@foreach($classroom as $item)
											<option value="{{ $item->id }}" data-title="{{ $item->title }}">{{ $item->title }}</option>
											@endforeach`);
			$('#price_schedule').val();
		}
	});
</script>
@endsection