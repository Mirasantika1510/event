@extends('layouts.app')
@section('content')

<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Payment Detail</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Payment Detail</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <a href="{{ route('admin.payments.history.index') }}"><i data-feather="chevrons-left"></i> Back To List Data Detail</a>
        <div class="card mt-2">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2"><img src="/app-assets/images/align-justify.svg"> Detail Payment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Ticket</th>
                                <td>{{ $payment->participant->ticket->name }}</td>
                            </tr>
                            <tr>
                                <th>Participant</th>
                                <td>{{ $payment->participant->name }}</td>
                            </tr>
                            <tr>
                                <th>No Invoice</th>
                                <td>{{ $payment->order_id }}</td>
                            </tr>
                            <tr>
                                <th>Payment Method</th>
                                <td>BANK TRANSFER</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>Rp. {{ number_format($payment->amount) }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ strtoupper($payment->status) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<!-- BEGIN: Page Vendor JS-->
<script src="/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" defer></script>
<script src="/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js" defer></script>

<script src="/app-assets/js/scripts/tables/table-datatables-advanced.js" defer></script>
<script src="/app-assets/js/scripts/components/components-dropdowns.js"></script>
<!-- END: Page Vendor JS-->
<!-- BEGIN: Page JS-->

@if(Session::get('success'))
<script type="text/javascript">
    $(document).ready(function(){

         // Success Type
         toastr['success']('Successfully Update or Delete Data.', 'Successfully', {
            closeButton: true,
            tapToDismiss: false,
            progressBar: true,
        });

     });
 </script>
 @endif
 @endsection