<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_attachments', function (Blueprint $table) {
            $table->id();
            $table->biginteger('schedule_id')->unsigned();
            $table->string('type');
            $table->text('file')->nullable();
            $table->string('video_name')->nullable();
            $table->text('youtube_url')->nullable();
            $table->timestamps();
            $table->foreign('schedule_id')->references('id')->on('schedules')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_attachments');
    }
}
