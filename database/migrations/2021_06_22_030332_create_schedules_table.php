<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->date('date');
            $table->time('time_start');
            $table->time('time_end');
            $table->text('description')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('pinned_message')->nullable();
            $table->enum('type', ['main_stage', 'classroom'])->nullable();
            $table->enum('platform_type', ['youtube', 'zoom'])->nullable();
            $table->text('youtube_url')->nullable();
            $table->boolean('youtube_playlist')->default(0);
            $table->string('zoom_id')->nullable();
            $table->string('zoom_password')->nullable();
            $table->string('zoom_api_key')->nullable();
            $table->string('zoom_api_secret')->nullable();
            $table->string('zoom_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
