<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polling_details', function (Blueprint $table) {
            $table->id();
            $table->biginteger('polling_id')->unsigned();
            $table->string('question');
            $table->text('options');
            $table->timestamps();
            $table->foreign('polling_id')->references('id')->on('pollings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polling_details');
    }
}
