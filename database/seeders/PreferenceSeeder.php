<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Preference;
use App\Models\User;

class PreferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  => 'Tests',
            'email' => 'user@gmail.com',
            'password'  => bcrypt('user123')
        ]);
    }
}
