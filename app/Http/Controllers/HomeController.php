<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Preference;
use Auth;

class HomeController extends Controller
{

    public function index()
    {
        $cek = Preference::where('user_id', Auth::id());

        if (!Preference::where('user_id', Auth::id())->where('category', 'event')->first()) {
            Preference::create([
                'user_id'   => Auth::id(),
                'action'    => [
                    "event_name" => "Event Name",
                    "date_start" => "2021-06-21",
                    "date_end" => "2021-06-22",
                    "timezone" => "Asia\/Jakart",
                    "primary_color" => "#24aff8",
                    "secondary_color" => "#06d6a0",
                    "tertiary_color" => "#ffb628",
                    'logo'  => 'default.jpg',
                    'icon'  => 'default.jpg',
                ],
                'category'      => 'event'
            ]);
        }

        if (!Preference::where('user_id', Auth::id())->where('category', 'zoom')->first()) {
           Preference::create([
            'user_id'   => Auth::id(),
            'action'    => [
                "zoom_api_key" => "",
                "zoom_api_secret" => "",
            ],
            'category'      => 'zoom'
        ]);       
       }

       if (!Preference::where('user_id', Auth::id())->where('category', 'landing')->first()) {
            Preference::create([
                'user_id'   => Auth::id(),
                'action'        => [
                    'title'         => '',
                    'description'   => '',
                    'footer_text'   => '',
                    'image'         => 'default.jpg'
                ],
                'category'      => 'landing'
            ]);
        }

        if (!Preference::where('user_id', Auth::id())->where('category', 'login')->first()) {
            Preference::create([
                'user_id'   => Auth::id(),
                'action'    => [
                    'title' => '',
                    'caption'   => '',
                    'title_register'    => '',
                    'caption_register'    => '',
                    'logo' => ''
                ],
                'category'      => 'login'
            ]);
        }

        if (!Preference::where('user_id', Auth::id())->where('category', 'payment')->first()) {
            Preference::create([
                'user_id'   => Auth::id(),
                'action'    => [
                    'ticket_title'                          => '',
                    'ticket_caption'                        => '',
                    'contact_email'                         => '',
                    'contact_name1'                         => '',
                    'contact_phone1'                        => '',
                    'contact_name2'                         => '',
                    'contact_phone2'                        => '',
                    'order_summary_title'                   => '',
                    'order_summary_caption'                 => '',
                    'payment_title'                         => '',
                    'payment_caption'                       => '',
                    'classroom_title'                         => '',
                    'classroom_caption'                       => '',
                    'payment_confirmation_title'            => '',
                    'payment_confirmation_caption'          => '',
                    'payment_bank_name'                     => '',
                    'payment_bank_branch'                   => '',
                    'payment_account_owner'                 => '',
                    'payment_account_number'                => '',
                ],
                'category'      => 'payment'
            ]);
        }

        if (!Preference::where('user_id', Auth::id())->where('category', 'web')->first()) {
            Preference::create([
                'user_id'   => Auth::id(),
                'action'    => [
                    'home_menu_text'                        => '',
                    'home_menu_icon_active'                 => '',
                    'home_menu_icon_nonactive'              => '',
                    'booth_menu_text'                       => '',
                    'booth_menu_icon_active'                => '',
                    'booth_menu_icon_nonactive'             => '',
                    'main_stage_menu_text'                  => '',
                    'main_stage_menu_icon_active'           => '',
                    'main_stage_menu_icon_nonactive'        => '',
                    'classroom_menu_text'                   => '',
                    'classroom_menu_icon_active'            => '',
                    'classroom_menu_icon_nonactive'         => '',
                    'schedule_icon'                         => '',
                    'sponsor_icon'                          => '',
                    'booth_plan_icon'                       => '',
                    'booth_list_icon'                       => '',
                    'booth_audio'                           => '',
                    'booth_banner'                          => '',
                    'booth_menu_status'                     => '',
                    'main_stage_menu_status'                => '',
                    'classroom_menu_status'                 => '',
                    'home_running_text'                     => '',
                    'home_slider_type'                      => '',
                    'home_background_image'                 => '',
                    'both_background_image'                 => '',
                    'main_stage_background_image'           => '',
                    'classroom_background_image'            => '',
                    
                ],
                'category'      => 'web'
            ]);
        }

        return view('welcome');
    }
}
