<?php

namespace App\Http\Controllers;

use App\Models\Booths;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BoothController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $booth = Booths::all();
            return DataTables::of($booth)->make(true);
        }

        return view('pages.booth.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        return view('pages.booth.action',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'icons' => 'required|max:2000'
        ]);

        if($file = $request->file('icons')){
            $name_file = $file->hashName();
            $destinasi = 'img/icons/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = null;
        }

        $data = [
            'name' => $request->name,
            'icons' => $name_file
        ];

        Booths::create($data);

        return redirect()->route('admin.booth.index')->with(['created' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Booths $booth)
    {
        $data  = $booth;
        return view('pages.booth.action',compact('data'));
    }

    public function update(Request $request, Booths $booth)
    {
        request()->validate([
            'name' => 'required',
            'icons' => 'max:2000'
        ]);

        if($file = $request->file('icons')){
            $name_file = $file->hashName();
            $destinasi = 'img/icons/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = null;
        }

        $data = [
            'name' => $request->name,
            'icons' => $name_file
        ];
        
        $booth->update($data);

        return redirect()->route('admin.booth.index')->with(['updated' => 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booths $booth)
    {
        $booth->delete();
        return response()->json();
    }
}
