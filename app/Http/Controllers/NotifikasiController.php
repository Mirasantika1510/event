<?php

namespace App\Http\Controllers;

use App\Models\Notifikasi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class NotifikasiController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $data = Notifikasi::all();
            return DataTables::of($data)->make(true);
        }
        $data = Notifikasi::all();
        // foreach($data as $item){
        //     if($item->send_at > date('Y-m-d H:i:s')){
        //         Notifikasi::find($item->id)->update(['status' => '2']);
        //     }
        // }
        return view('pages.notifikasi.index',compact('data'));
    }

    public function create()
    {
        $data = null;
        return view('pages.notifikasi.action',compact('data'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'message' => 'required',
            'send_at' => 'required',
        ]);

        $data = [
            'message' => $request->message,
            'send_at' => $request->send_at,
            'status' => 1
        ];

        Notifikasi::create($data);
        return redirect()->route('admin.notif.index')->with(['created' => 'created']);
    }

    public function update(Notifikasi $notif,Request $request)
    {
        request()->validate([
            'message' => 'required',
            'send_at' => 'required',
        ]);

        $data = [
            'message' => $request->message,
            'send_at' => $request->send_at,
        ];

        $notif->update($data);
        return redirect()->route('admin.notif.index')->with(['updated' => 'updated']);
    }

    public function edit(Notifikasi $notif){
        $data = $notif;
        return view('pages.notifikasi.action',compact('data'));
    }

    public function delete(Notifikasi $notif)
    {
        $notif->delete();
        return response()->json();
    }
}
