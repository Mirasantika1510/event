<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Preference;
use App\Models\Speaker;
use Illuminate\Http\FileHelpers;
use Session;
use Auth;

class PreferencesController extends Controller
{
    public function index()
    {
        return view('pages.preferences.index');
    }

    public function event()
    {
        $event = Preference::where('category', 'event')->where('user_id', Auth::id())->first();
        return view('pages.preferences.event', compact('event'));
    }

    public function eventUpdate(Request $request, Preference $preferences)
    {
        $file = $request->file('logo');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/event/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = $preferences->action['logo'];
        }

        $file2 = $request->file('icon');
        if ($file2) {
            $icon = $file2->hashName();
            $destinasi = 'img/event/';
            $file2->move($destinasi, $icon);
        } else {
            $icon = $preferences->action['icon'];
        }

       $preferences->update([
            'action'    => [
                'event_name' => $request->event_name,
                'date_start' => $request->date_start,
                'date_end' => $request->date_end,
                'timezone' => $request->timezone,
                'primary_color' => $request->primary_color,
                'secondary_color' => $request->secondary_color,
                'tertiary_color' => $request->tertiary_color,
                'logo'  => $name_file,
                'icon'  => $icon
            ],
            'category'      => 'event'
        ]);

        Session::flash('success');
        return redirect()->back();
    }

    public function zoom()
    {
        $zoom = Preference::where('category', 'zoom')->where('user_id', Auth::id())->first();
        return view('pages.preferences.zoom', compact('zoom'));
    }

    public function zoomUpdate(Request $request, Preference $preferences)
    {
       $preferences->update([
            'action'        => $request->only('zoom_api_key', 'zoom_api_secret'),
            'category'      => 'zoom'
        ]);

        Session::flash('success');

        return redirect()->back();
    }

    public function landing()
    {
        $landing = Preference::where('category', 'landing')->where('user_id', Auth::id())->first();
        $speaker = Speaker::where('user_id', 1)->get();
        return view('pages.preferences.landing', compact('landing', 'speaker'));
    }

    public function landingUpdate(Request $request, Preference $preferences)
    {
        $request->validate([
            'image' => 'mimes:jpg,png,jpeg'
        ]);

        $file = $request->file('image');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/landing/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = $preferences->action['image'];
        }

       $preferences->update([
            'user_id'       =>1,
            'action'        => [
                'title'         => $request->title,
                'description'   => $request->description,
                'footer_text'   => $request->footer_text,
                'image'         => $name_file
            ],
            'category'      => 'landing'
        ]);

        Session::flash('success');
        return redirect()->back();
    }


    public function speakerInsert(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpg,png,jpeg|max:500'
        ]);

        $file = $request->file('image');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/speaker/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = 'default.jpg';
        }

        Speaker::create([
            'user_id'   => 1,
            'name'      => $request->name,
            'title'     => $request->title,
            'image'     => $name_file
        ]);

        Session::flash('success');

        return redirect()->back();
    }

    public function speakerDelete(Speaker $speaker)
    {
        $speaker->delete();
        Session::flash('success');
        return redirect()->back();

    }

    public function login()
    {
        $data = Preference::where('category', 'login')->where('user_id', Auth::id())->first();
        return view('pages.preferences.login', compact('data'));
    }

    public function loginUpdate(Request $request, Preference $preferences)
    {
        $file = $request->file('logo');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/login/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = $preferences->action['logo'];
        }

        $preferences->update([
            'action'    => [
                'title' => $request->title,
                'caption'   => $request->caption,
                'title_register'    => $request->title_register,
                'caption_register'    => $request->caption_register,
                'logo' => $name_file
            ],
        ]);

        Session::flash('success');

        return redirect()->back();
    }

    public function payment()
    {
        $event = Preference::where('category', 'payment')->where('user_id', Auth::id())->first();
        return view('pages.preferences.payment', compact('event'));
    }

    public function paymentUpdate(Request $request, Preference $preferences)
    {

        $preferences->update([
            'action'    => $request->only('ticket_title' , 'ticket_caption',  'contact_email', 'contact_name1', 'contact_phone1',  'contact_name2', 'contact_phone2',  'order_summary_title', 'order_summary_caption' , 'payment_title', 'payment_caption' , 'classroom_title', 'classroom_caption' , 'payment_confirmation_title',  'payment_confirmation_caption', 'payment_bank_name',  'payment_bank_branch', 'payment_account_owner' , 'payment_account_number'),
        ]);

        Session::flash('success');

        return redirect()->back();
    }

    public function web()
    {
        $event = Preference::where('category', 'web')->where('user_id', Auth::id())->first();
        return view('pages.preferences.web', compact('event'));
    }

    public function webUpdate(Request $request, Preference $preferences)
    {
        $file1 = $request->file('home_menu_icon_active');
        if ($file1) {
            $home_menu_icon_active = $file1->hashName();
            $destinasi = 'img/landing/';
            $file1->move($destinasi, $home_menu_icon_active);
        } else {
            $home_menu_icon_active = $preferences->action['home_menu_icon_active'];
        }

        $file2 = $request->file('home_menu_icon_nonactive');
        if ($file2) {
            $home_menu_icon_nonactive = $file2->hashName();
            $destinasi = 'img/landing/';
            $file2->move($destinasi, $home_menu_icon_nonactive);
        } else {
            $home_menu_icon_nonactive = $preferences->action['home_menu_icon_nonactive'];
        }

        $file3 = $request->file('booth_menu_icon_active');
        if ($file3) {
            $booth_menu_icon_active = $file3->hashName();
            $destinasi = 'img/landing/';
            $file3->move($destinasi, $booth_menu_icon_active);
        } else {
            $booth_menu_icon_active = $preferences->action['booth_menu_icon_active'];
        }

        $file4 = $request->file('booth_menu_icon_nonactive');
        if ($file4) {
            $booth_menu_icon_nonactive = $file4->hashName();
            $destinasi = 'img/landing/';
            $file4->move($destinasi, $booth_menu_icon_nonactive);
        } else {
            $booth_menu_icon_nonactive = $preferences->action['booth_menu_icon_nonactive'];
        }

        $file5 = $request->file('main_stage_menu_icon_active');
        if ($file5) {
            $main_stage_menu_icon_active = $file5->hashName();
            $destinasi = 'img/landing/';
            $file5->move($destinasi, $main_stage_menu_icon_active);
        } else {
            $main_stage_menu_icon_active = $preferences->action['main_stage_menu_icon_active'];
        }

        $file6 = $request->file('main_stage_menu_icon_nonactive');
        if ($file6) {
            $main_stage_menu_icon_nonactive = $file6->hashName();
            $destinasi = 'img/landing/';
            $file6->move($destinasi, $main_stage_menu_icon_nonactive);
        } else {
            $main_stage_menu_icon_nonactive = $preferences->action['main_stage_menu_icon_nonactive'];
        }

        $file7 = $request->file('classroom_menu_icon_active');
        if ($file7) {
            $classroom_menu_icon_active = $file7->hashName();
            $destinasi = 'img/landing/';
            $file7->move($destinasi, $classroom_menu_icon_active);
        } else {
            $classroom_menu_icon_active = $preferences->action['classroom_menu_icon_active'];
        }

        $file8 = $request->file('classroom_menu_icon_nonactive');
        if ($file8) {
            $classroom_menu_icon_nonactive = $file8->hashName();
            $destinasi = 'img/landing/';
            $file8->move($destinasi, $classroom_menu_icon_nonactive);
        } else {
            $classroom_menu_icon_nonactive = $preferences->action['classroom_menu_icon_nonactive'];
        }

        $file9 = $request->file('schedule_icon');
        if ($file9) {
            $schedule_icon = $file9->hashName();
            $destinasi = 'img/landing/';
            $file9->move($destinasi, $schedule_icon);
        } else {
            $schedule_icon = $preferences->action['schedule_icon'];
        }

        $file10 = $request->file('sponsor_icon');
        if ($file10) {
            $sponsor_icon = $file10->hashName();
            $destinasi = 'img/landing/';
            $file10->move($destinasi, $sponsor_icon);
        } else {
            $sponsor_icon = $preferences->action['sponsor_icon'];
        }

        $file11 = $request->file('booth_plan_icon');
        if ($file11) {
            $booth_plan_icon = $file11->hashName();
            $destinasi = 'img/landing/';
            $file11->move($destinasi, $booth_plan_icon);
        } else {
            $booth_plan_icon = $preferences->action['booth_plan_icon'];
        }

        $file12 = $request->file('booth_list_icon');
        if ($file12) {
            $booth_list_icon = $file12->hashName();
            $destinasi = 'img/landing/';
            $file12->move($destinasi, $booth_list_icon);
        } else {
            $booth_list_icon = $preferences->action['booth_list_icon'];
        }

        $file13 = $request->file('booth_audio');
        if ($file13) {
            $booth_audio = $file13->hashName();
            $destinasi = 'img/landing/';
            $file13->move($destinasi, $booth_audio);
        } else {
            $booth_audio = $preferences->action['booth_audio'];
        }

        $file14 = $request->file('booth_banner');
        if ($file14) {
            $booth_banner = $file14->hashName();
            $destinasi = 'img/landing/';
            $file14->move($destinasi, $booth_banner);
        } else {
            $booth_banner = $preferences->action['booth_banner'];
        }

        $file15 = $request->file('home_background_image');
        if ($file15) {
            $home_background_image = $file15->hashName();
            $destinasi = 'img/landing/';
            $file15->move($destinasi, $home_background_image);
        } else {
            $home_background_image = $preferences->action['home_background_image'];
        }

        $file16 = $request->file('both_background_image');
        if ($file16) {
            $both_background_image = $file16->hashName();
            $destinasi = 'img/landing/';
            $file16->move($destinasi, $both_background_image);
        } else {
            $both_background_image = $preferences->action['both_background_image'];
        }

        $file17 = $request->file('main_stage_background_image');
        if ($file17) {
            $main_stage_background_image = $file17->hashName();
            $destinasi = 'img/landing/';
            $file17->move($destinasi, $main_stage_background_image);
        } else {
            $main_stage_background_image = $preferences->action['main_stage_background_image'];
        }

        $file18 = $request->file('classroom_background_image');
        if ($file18) {
            $classroom_background_image = $file18->hashName();
            $destinasi = 'img/landing/';
            $file18->move($destinasi, $classroom_background_image);
        } else {
            $classroom_background_image = $preferences->action['classroom_background_image'];
        }

        $preferences->update([
            'action'    => [
                    'home_menu_text'                        => $request->home_menu_text,
                    'home_menu_icon_active'                 => $home_menu_icon_active,
                    'home_menu_icon_nonactive'              => $home_menu_icon_nonactive,
                    'booth_menu_text'                       => $request->booth_menu_text,
                    'booth_menu_icon_active'                => $booth_menu_icon_active,
                    'booth_menu_icon_nonactive'             => $booth_menu_icon_nonactive,
                    'main_stage_menu_text'                  => $request->main_stage_menu_text,
                    'main_stage_menu_icon_active'           => $main_stage_menu_icon_active,
                    'main_stage_menu_icon_nonactive'        => $main_stage_menu_icon_nonactive,
                    'classroom_menu_text'                   => $request->classroom_menu_text,
                    'classroom_menu_icon_active'            => $classroom_menu_icon_active,
                    'classroom_menu_icon_nonactive'         => $classroom_menu_icon_nonactive,
                    'schedule_icon'                         => $schedule_icon,
                    'sponsor_icon'                          => $sponsor_icon,
                    'booth_plan_icon'                       => $booth_plan_icon,
                    'booth_list_icon'                       => $booth_list_icon,
                    'booth_audio'                           => $booth_audio,
                    'booth_banner'                          => $booth_banner,
                    'booth_menu_status'                     => $request->booth_menu_status,
                    'main_stage_menu_status'                => $request->main_stage_menu_status,
                    'classroom_menu_status'                 => $request->classroom_menu_status,
                    'home_running_text'                     => $request->home_running_text,
                    'home_slider_type'                      => $request->home_slider_type,
                    'home_background_image'                 => $home_background_image,
                    'both_background_image'                 => $both_background_image,
                    'main_stage_background_image'           => $main_stage_background_image,
                    'classroom_background_image'            => $classroom_background_image,
                ],
        ]);

        Session::flash('success');

        return redirect()->back();
    }
}
