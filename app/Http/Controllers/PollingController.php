<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use App\Models\Ticket;
use App\Models\Polling;
use App\Models\PollingDetail;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PollingController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $data = Polling::latest()->get();
            return DataTables::of($data)->make(true);
        }
        return view('pages.pollings.index');
    }

    public function create()
    {
        $data       = null;
        $pollings   = null;
        return view('pages.pollings.action',compact('data', 'pollings'));
    }

    public function store(Request $request)
    {
        $polling = Polling::create([
            'name'      => $request->name,
            'status'    => 'active'
        ]);

        $number = count($request->question);
        for ($i=0; $i < $number; $i++) { 
            PollingDetail::create([
                'polling_id'    => $polling->id,
                'question'      => $request->question[$i],
                'options'       => $request->option[$i]
            ]);
        }

        Session::flash('success');
        return redirect()->route('admin.polling.index');
    }

    public function delete(Polling $polling)
    {
        $polling->delete();
        return response()->json();
    }

    public function edit(Polling $polling)
    {
        $data       = $polling;
        $pollings   = PollingDetail::where('polling_id', $polling->id)->get();
        return view('pages.pollings.action',compact('data', 'pollings'));
    }

    public function update(Request $request, Polling $polling)
    {

        $polling->update([
            'name'      => $request->name
        ]);

        PollingDetail::where('polling_id', $polling->id)->delete();

        $number = count($request->question);
        for ($i=0; $i < $number; $i++) { 
            PollingDetail::create([
                'polling_id'    => $polling->id,
                'question'      => $request->question[$i],
                'options'       => $request->option[$i]
            ]);
        }

        Session::flash('success');
        return redirect()->route('admin.polling.index');
    }

}
