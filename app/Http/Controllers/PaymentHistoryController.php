<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use App\Models\Ticket;
use App\Models\Payment;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Models\PaymentConfirmation;
use Yajra\DataTables\Facades\DataTables;

class PaymentHistoryController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $data = Payment::latest()->get();
            return DataTables::of($data)
            ->editColumn('ticket_id', function($data) {
                return $data->participant->ticket->name;
            })
            ->make(true);
        }
        return view('pages.payments.history');
    }

    public function detail(Payment $payment)
    {
        if(request()->ajax()){
            $schedule_id = explode(',', $payment->participant->schedule_id);
            $data = Schedule::whereIn('id', $schedule_id)->latest()->get();
            return DataTables::of($data)
            ->editColumn('price', function($data) use ($payment) {
                return "Rp.".number_format($payment->participant->ticket->price);
            })
            ->make(true);
        }
         return view('pages.payments.history_detail', compact('payment'));
    }

    public function data(Payment $payment)
    {
        return view('pages.payments.lihat', compact('payment'));
    }

    public function confirmation(Payment $payment)
    {
        if(request()->ajax()){
            $data = PaymentConfirmation::where('payment_id', $payment->id)->latest()->get();
            return DataTables::of($data)
            ->editColumn('order_id', function($data) {
                return $data->payment->order_id;
            })
            ->editColumn('account_name', function($data) {
                return $data->participant->name;
            })
            ->editColumn('amount', function($data) {
                return $data->payment->amount;
            })
            ->editColumn('created_at', function($data) {
                return Carbon::parse($data->created_at)->format('Y-m-d');
            })
            ->make(true);
        }

        return view('pages.payments.confirmation_detail', compact('payment'));
    }
}
