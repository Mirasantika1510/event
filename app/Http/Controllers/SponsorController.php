<?php

namespace App\Http\Controllers;

use App\Models\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gold()
    {
        if(request()->ajax()){
            $data = Sponsor::where('category',1)->get();
            return DataTables::of($data)->make(true);
        }

        return view('pages.sponsor.gold');
    }

    public function silver()
    {
        if(request()->ajax()){
            $data = Sponsor::where('category',2)->get();
            return DataTables::of($data)->make(true);
        }

        return view('pages.sponsor.silver');
    }

    public function bronze()
    {
        if(request()->ajax()){
            $data = Sponsor::where('category',3)->get();
            return DataTables::of($data)->make(true);
        }

        return view('pages.sponsor.bronze');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $sort = null;
        return view('pages.sponsor.action',compact('data','sort'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => 'required|max:2000',
            'link' => 'required|url',
            'category' => 'required',
        ]);

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/sponsor/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = null;
        }

        $sponsor = Sponsor::where('category',$request->category)->latest('sort')->first();
        if($sponsor !== null){
            $no = $sponsor->sort;
        }else{
            $no = 0;
        }

        $data = [
            'image' => $name_file,
            'link' => $request->link,
            'category' => $request->category,
            'sort' => $no + 1,
        ];

        Sponsor::create($data);

        if($request->category == 1){
            return redirect()->route('admin.sponsor.gold')->with(['created' => 'created']);
        }elseif($request->category == 2){
            return redirect()->route('admin.sponsor.silver')->with(['created' => 'created']);
        }else{
            return redirect()->route('admin.sponsor.bronze')->with(['created' => 'created']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor)
    {
        $data = $sponsor;
        $sort = Sponsor::where('category',$sponsor->category)->get();

        return view('pages.sponsor.action',compact('data','sort'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sponsor $sponsor)
    {
        request()->validate([
            'link' => 'required|url',
            'category' => 'required',
        ]);

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/sponsor/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = $sponsor->image;
        }

        $sponsor = Sponsor::where('category',$request->category)->latest('sort')->first();
        if($sponsor !== null){
            $no = $sponsor->sort;
        }else{
            $no = 0;
        }


        Sponsor::where('category',$sponsor->category)->where('sort',$request->sort)->update(['sort' => $sponsor->sort]);

        $data = [
            'image' => $name_file,
            'link' => $request->link,
            'category' => $request->category,
            'sort' => $request->sort,
        ];

        $sponsor->update($data);

        if($sponsor->category == 1){
            return redirect()->route('admin.sponsor.gold')->with(['updated' => 'updated']);
        }elseif($sponsor->category == 2){
            return redirect()->route('admin.sponsor.silver')->with(['updated' => 'updated']);
        }else{
            return redirect()->route('admin.sponsor.bronze')->with(['updated' => 'updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sponsor $sponsor)
    {
        $no = $sponsor->sort;

        $sortUp = Sponsor::where('category',$sponsor->category)->where('sort',$no+1)->update(['sort' => $sponsor->sort]);

        File::delete(public_path('img/sponsor/',$sponsor->image));
        $sponsor->delete();

        return response()->json();
    }
}
