<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Session;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Models\PaymentConfirmation;
use App\Models\Payment;
use App\Models\Schedule;
use Yajra\DataTables\Facades\DataTables;

class PaymentConfirmationController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $data = PaymentConfirmation::latest()->get();
            return DataTables::of($data)
            ->editColumn('order_id', function($data) {
                return $data->payment->order_id;
            })
            ->editColumn('account_name', function($data) {
                return $data->participant->name;
            })
            ->editColumn('amount', function($data) {
                return $data->payment->amount;
            })
            ->editColumn('created_at', function($data) {
                return Carbon::parse($data->created_at)->format('Y-m-d');
            })
            ->make(true);
        }

        return view('pages.payments.confirmation');
    }

    public function approved(PaymentConfirmation $payment)
    {
        $payment->update([
            'status'    => 'approved'
        ]);

        Session::flash('success');
        return redirect()->back();
    }

    public function rejected(PaymentConfirmation $payment)
    {
        $payment->update([
            'status'    => 'rejected'
        ]);

        Session::flash('success');
        return redirect()->back();
    }
}
