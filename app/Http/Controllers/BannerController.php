<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function image()
    {
        if(request()->ajax()){
            $data = Banner::where('status',1)->orderBy('order','ASC')->get();
            return DataTables::of($data)->make(true);
        }

        return view('pages.banner.image');
    }

    public function youtube()
    {
        if(request()->ajax()){
            $data = Banner::where('status',2)->orderBy('order','ASC')->get();
            return DataTables::of($data)->make(true);
        }

        return view('pages.banner.youtube');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $order = null;

        return view('pages.banner.action',compact('data','order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->status == 1){
            request()->validate([
                'status' => 'required',
                'image' => 'required|max:4000',
            ]);
        }else{
            request()->validate([
                'status' => 'required',
                'url' => 'required|url',
            ]);
        }

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/banner/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = null;
        }

        $banner = Banner::where('status',$request->status)->latest('order')->first();
        if($banner !== null){
            $no = $banner->order;
        }else{
            $no = 0;
        }

        $data = [
            'image' => $name_file,
            'url' => $request->url,
            'status' => $request->status,
            'order' => $no + 1,
        ];
        

        $status = Banner::create($data)->status;

        if($status == 1){
            return redirect()->route('admin.banner.image');
        }else{
            return redirect()->route('admin.banner.youtube');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        $data = $banner;
        $order = Banner::where('status',$banner->status);
        return view('pages.banner.action',compact('data','order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/banner/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = $banner->image;
        }

        $data = [
            'image' => $name_file,
            'url' => $request->url,
            'status' => $request->status,
            'order' => $request->order,
        ];
        
        Banner::where('status',$banner->status)->where('order',$request->order)->update(['order' => $banner->order]);

        $banner->update($data);

        if($banner->status == 1){
            return redirect()->route('admin.banner.image');
        }else{
            return redirect()->route('admin.banner.youtube');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $no = $banner->order;

        $sortUp = Banner::where('status',$banner->status)->where('order',$no+1)->update(['order' => $banner->order]);

        File::delete(public_path('img/banner/',$banner->image));
        $banner->delete();

        return response()->json();
    }
}
