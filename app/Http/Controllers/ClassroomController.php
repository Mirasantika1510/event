<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Http\FileHelpers;
use App\Models\ScheduleAttachment;
use Yajra\DataTables\Facades\DataTables;

class ClassroomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        if(request()->ajax()){
            $data = Schedule::where('type', 'classroom')->latest()->get();
            return DataTables::of($data)->make(true);
        }
        return view('pages.classroom.index');
    }

    public function create()
    {
        $data = null;
        return view('pages.classroom.action',compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'             => 'max:150',
            'description'       => 'max:1000',
            'pinned_message'    => 'max:255',
        ]);

        $file = $request->file('thumbnail');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/schedule/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = 'default.jpg';
        }

        Schedule::create([
            'title'             => $request->name,
            'thumbnail'         => $name_file,
            'date'              => $request->date,
            'time_start'        => $request->time_start,
            'time_end'          => $request->time_end,
            'description'       => $request->description,
            'pinned_message'    => $request->pinned_message,
            'type'              => 'classroom',
            'platform_type'     => $request->platform_type,
            'youtube_url'       => $request->youtube_url ? : null,
            'youtube_playlist'  => $request->validationRadioBootstrap ? : 0,
            'zoom_id'           => $request->zoom_id ? : null,
            'zoom_password'     => $request->zoom_password ? : null,
            'zoom_api_key'      => $request->zoom_api_key ? : null,
            'zoom_api_secret'   => $request->zoom_api_secret ? : null,
            'zoom_url'          => $request->zoom_url ? : null,

        ]);

        Session::flash('success');
        return redirect()->route('admin.schedule.classroom.index');
    }

    public function delete(Schedule $classroom)
    {
        $classroom->delete();
        return response()->json();
    }

    public function edit(Schedule $classroom)
    {
        $data = $classroom;
        return view('pages.classroom.action',compact('data'));
    }

    public function update(Request $request, Schedule $classroom)
    {
        $request->validate([
            'title'             => 'max:150',
            'description'       => 'max:1000',
            'pinned_message'    => 'max:255',
        ]);

        $file = $request->file('thumbnail');
        if ($file) {
            $name_file = $file->hashName();
            $destinasi = 'img/schedule/';
            $file->move($destinasi, $name_file);
        } else {
            $name_file = $classroom->thumbnail;
        }

        $classroom->update([
            'title'             => $request->name,
            'thumbnail'         => $name_file,
            'date'              => $request->date,
            'time_start'        => $request->time_start,
            'time_end'          => $request->time_end,
            'description'       => $request->description,
            'pinned_message'    => $request->pinned_message,
            'type'              => 'classroom',
            'platform_type'     => $request->platform_type,
            'youtube_url'       => $request->youtube_url ? : $classroom->youtube_url,
            'youtube_playlist'  => $request->validationRadioBootstrap ? : $classroom->youtube_playlist,
            'zoom_id'           => $request->zoom_id ? : $classroom->zoom_id,
            'zoom_password'     => $request->zoom_password ? : $classroom->zoom_password,
            'zoom_api_key'      => $request->zoom_api_key ? : $classroom->zoom_api_key,
            'zoom_api_secret'   => $request->zoom_api_secret ? : $classroom->zoom_api_secret,
            'zoom_url'          => $request->zoom_url ? : $classroom->zoom_url,

        ]);

        Session::flash('success');
        return redirect()->route('admin.schedule.classroom.index');
    }

    public function attachment(Schedule $classroom)
    {
        if(request()->ajax()){
            $data = ScheduleAttachment::where('schedule_id', $classroom->id)->get();
            return DataTables::of($data)->make(true);
        }
        return view('pages.classroom.attachment',compact('classroom'));
    }

    // ATTACHMENT
    public function attachmentCreate(Schedule $classroom)
    {
        $data = null;
        return view('pages.classroom.action_attachment',compact('data', 'classroom'));
    }

    public function attachmentStore(Schedule $classroom, Request $request)
    {
        if ($request->customRadio == 'document') {
            $request->validate([
                'file'  => 'mimes:pdf,docx,odt,xls,xlsx,pptx|max:2000'
            ]);
        } elseif ($request->customRadio == 'image') {
            $request->validate([
                'file'  => 'mimes:jpg,jpeg,png|max:1000'
            ]);
        }

        $file = $request->file('file');
        if ($file) {
            $name_file = $file->hashName();
            if ($request->customRadio == 'document') {
                $destinasi = 'file/schedule';
            } else {
                $destinasi = 'img/schedule';            
            }
            $file->move($destinasi, $name_file);
        } else {
            $name_file = null;
        }

        ScheduleAttachment::create([
            'schedule_id'       => $classroom->id,
            'type'              => $request->customRadio,
            'file'              => $name_file,
            'video_name'        => $request->video_name ? : null,
            'youtube_url'       => $request->youtube_url ? : null
        ]);

        Session::flash('success');

        return redirect()->route('admin.schedule.classroom.attachment', $classroom->id);
    }

    public function attachmentEdit(ScheduleAttachment $classroom)
    {
        $data = $classroom;
        return view('pages.classroom.action_attachment',compact('data'));
    }

    public function attachmentUpdate(ScheduleAttachment $classroom, Request $request)
    {
        if ($request->customRadio == 'document') {
            $request->validate([
                'file'  => 'mimes:pdf,docx,odt,xls,xlsx,pptx|max:2000'
            ]);
        } elseif ($request->customRadio == 'image') {
            $request->validate([
                'file'  => 'mimes:jpg,jpeg,png|max:1000'
            ]);
        }

        $file = $request->file('file');
        if ($file) {
            $name_file = $file->hashName();
            if ($request->customRadio == 'document') {
                $destinasi = 'file/schedule';
            } else {
                $destinasi = 'img/schedule';            
            }
            $file->move($destinasi, $name_file);
        } else {
            $name_file = $classroom->file;
        }

        if ($request->customRadio == 'document') {
            $classroom->update([
                'type'              => $request->customRadio,
                'file'              => $name_file,
                'video_name'        => null,
                'youtube_url'       => null
            ]);
        } elseif ($request->customRadio == 'image') {
            $classroom->update([
                'type'              => $request->customRadio,
                'file'              => $name_file,
                'video_name'        => null,
                'youtube_url'       => null
            ]);
        } else {
            $classroom->update([
                'type'              => $request->customRadio,
                'file'              => null,
                'video_name'        => $request->video_name ? : $classroom->video_name,
                'youtube_url'       => $request->youtube_url ? : $classroom->youtube_url
            ]);
        }

        Session::flash('success');

        return redirect()->route('admin.schedule.classroom.attachment', $classroom->schedule_id);
    }

    public function attachmentDelete(ScheduleAttachment $classroom)
    {
        $classroom->delete();
        return response()->json();
    }
}
