<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Ticket;
use App\Models\Schedule;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function web()
    {
        if(request()->ajax()){
            $data = Participant::where('type',1)->get();
            return DataTables::of($data)
            ->editColumn('ticket_id', function($data) {
                return $data->ticket->name;
            })
            ->editColumn('city_id', function($data) {
                return $data->city->nama_kota;
            })
            ->editColumn('schedule_id', function($data) {
                return $data->schedule->title;
            })
            ->make(true);
        }

        return view('pages.participants.web-regis');
    }

    public function invite()
    {
        if(request()->ajax()){
            $data = Participant::where('type',2)->get();
            return DataTables::of($data)
            ->editColumn('ticket_id', function($data) {
                return $data->ticket->name;
            })
            ->editColumn('city_id', function($data) {
                return $data->city->nama_kota;
            })
            ->editColumn('schedule_id', function($data) {
                return $data->schedule->title;
            })
            ->make(true);
        }

        return view('pages.participants.invite');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $city = City::all();
        $scedule = Schedule::all();
        $ticket = Ticket::all();

        return view('pages.participants.action',compact('data','city','scedule','ticket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'          => 'required',
            'email'         => 'required',
            'password'      => 'required|min:7',
            'phone'         => 'required',
            'company'       => 'required',
            'jobs'          => 'required',
            'type'          => 'required',
            'ticket_id'     => 'required',
            'city_id'       => 'required',
            'schedule_id'   => 'required',
            'image'         => 'required|max:3000',
        ]);

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/participant/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = null;
        }

        $data = [
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
            'phone'         => $request->phone,
            'company'       => $request->company,
            'jobs'          => $request->jobs,
            'type'          => $request->type,
            'ticket_id'     => $request->ticket_id,
            'city_id'       => $request->city_id,
            'schedule_id'   => implode(',',$request->schedule_id),
            'image'         => $name_file,
        ];

        Participant::create($data);

        if($request->type == 1){
            return redirect()->route('admin.participant.web-regis')->with(['created' => 'created']);
        }else{
            return redirect()->route('admin.participant.invite')->with(['created' => 'created']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Participant $participant)
    {
        return view('pages.participants.detail',compact('participant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Participant $participant)
    {
        $data = $participant;
        $city = City::all();
        $scedule = Schedule::all();
        $ticket = Ticket::all();

        return view('pages.participants.action',compact('data','city','scedule','ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participant $participant)
    {
        request()->validate([
            'name'          => 'required',
            'email'         => 'required',
            'phone'         => 'required',
            'company'       => 'required',
            'jobs'          => 'required',
            'type'          => 'required',
            'ticket_id'     => 'required',
            'city_id'       => 'required',
            'schedule_id'   => 'required',
        ]);

        if($file = $request->file('image')){
            $name_file = $file->hashName();
            $destinasi = 'img/participant/';
            $file->move($destinasi, $name_file);
        }else{
            $name_file = $participant->image;
        }

        $data = [
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'company'       => $request->company,
            'jobs'          => $request->jobs,
            'type'          => $request->type,
            'ticket_id'     => $request->ticket_id,
            'city_id'       => $request->city_id,
            'schedule_id'   => implode(',',$request->schedule_id),
            'image'         => $name_file,
        ];


        $participant->update($data);

        if($participant->type == 1){
            return redirect()->route('admin.participant.web-regis')->with(['updated' => 'updated']);
        }else{
            return redirect()->route('admin.participant.invite')->with(['updated' => 'updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participant $participant)
    {
        File::delete(public_path('img/participant/',$participant->image));
        $participant->delete();
    }
}
