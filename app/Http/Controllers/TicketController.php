<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Models\TicketSchedule;
use App\Models\Schedule;
use Yajra\DataTables\Facades\DataTables;

class TicketController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $data = Ticket::latest()->get();
            return DataTables::of($data)->make(true);
        }
        return view('pages.ticket.index');
    }

    public function create()
    {
        $data = null;
        $classroom = Schedule::where('type', 'classroom')->get();
        return view('pages.ticket.action',compact('data', 'classroom'));
    }

    public function store(Request $request)
    {
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->type,
            'price' => $request->price,
            'ticket_only' => $request->validationRadioBootstrap
        ];

        $schedule = Ticket::create($data);

        if ($request->validationRadioBootstrap == '0') {
            for ($i=0; $i < count($request->schedule_id); $i++) { 
               TicketSchedule::create([
                    'ticket_id'     => $schedule->id,
                    'schedule_id'   => $request->schedule_id[$i],
                    'price'         => $request->schedule_price[$i]
               ]);
            }
        }
        Session::flash('success');
        return redirect()->route('admin.ticket.index');
    }

    public function delete(Ticket $ticket)
    {
        $ticket->delete();
        return response()->json();
    }

    public function edit(Ticket $ticket)
    {
        $data = $ticket;
        $classroom = Schedule::where('type', 'classroom')->get();
        $ticketList = TicketSchedule::where('ticket_id', $ticket->id)->get();
        return view('pages.ticket.action',compact('data', 'classroom', 'ticketList'));
    }

    public function update(Request $request, Ticket $ticket)
    {

        $ticket->update([
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->type,
            'price' => $request->price,
            'ticket_only' => $request->validationRadioBootstrap
        ]);

        if ($request->validationRadioBootstrap == '0') {
            TicketSchedule::where('ticket_id', $ticket->id)->delete();
            for ($i=0; $i < count($request->schedule_id); $i++) { 
               TicketSchedule::create([
                    'ticket_id'     => $ticket->id,
                    'schedule_id'   => $request->schedule_id[$i],
                    'price'         => $request->schedule_price[$i]
               ]);
            }
        }

        Session::flash('success');
        return redirect()->route('admin.ticket.index');
    }

    public function schedule($id)
    {
        $data = Schedule::whereId($id)->first();

        return response()->json($data);
    }

    public function scheduleDelete(TicketSchedule $ticket)
    {
        $ticket->delete();

        Session::flash('success');
        return redirect()->back();
    }
}
