<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;
    protected $table = 'schedules';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function participant()
    {
        return $this->hasMany(Participant::class);
    }
}
