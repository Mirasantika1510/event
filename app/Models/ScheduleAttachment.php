<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleAttachment extends Model
{
    use HasFactory;
     protected $table = 'schedule_attachments';

    protected $dates = ['deleted_at'];

    protected $guarded = [];
}
