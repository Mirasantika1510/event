<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $table = 'payments';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function participant()
    {
        return $this->belongsTo(Participant::class);
    }
}
