<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketSchedule extends Model
{
    use HasFactory;
     protected $table = 'ticket_schedules';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }

}