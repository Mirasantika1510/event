<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
    use HasFactory;

    protected $table = 'payment_confirmations';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function participant()
    {
        return $this->belongsTo(Participant::class);
    }
}
