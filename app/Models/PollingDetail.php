<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PollingDetail extends Model
{
    use HasFactory;
    protected $table = 'polling_details';

    protected $dates = ['deleted_at'];

    protected $guarded = [];
}
