<?php

use App\Models\Participant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoothController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\MainStageController;
use App\Http\Controllers\NotifikasiController;
use App\Http\Controllers\ParticipantController;
use App\Http\Controllers\PreferencesController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaymentHistoryController;
use App\Http\Controllers\PaymentConfirmationController;
use App\Http\Controllers\SponsorController;
use App\Http\Controllers\PollingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware('auth')->group(function() {
    Route::get('/' , [HomeController::class , 'index'])->name('index');

    Route::group(['prefix' => 'admin' , 'as' => 'admin.'], function () {
        Route::group(['prefix' => 'preferences' , 'as' => 'preferences.'], function () {
            Route::get('/' , [PreferencesController::class , 'index'])->name('index');
            Route::get('/event' , [PreferencesController::class , 'event'])->name('event');
            Route::post('/event/update/{preferences}' , [PreferencesController::class , 'eventUpdate'])->name('event-update');
            Route::get('/zoom' , [PreferencesController::class , 'zoom'])->name('zoom');
            Route::post('/zoom/update/{preferences}' , [PreferencesController::class , 'zoomUpdate'])->name('zoom-update');

            Route::get('/landing-page' , [PreferencesController::class , 'landing'])->name('landing');
            Route::post('/landing/update/{preferences}' , [PreferencesController::class , 'landingUpdate'])->name('landing-update');

            Route::get('/login' , [PreferencesController::class , 'login'])->name('login');
            Route::post('/login/update/{preferences}' , [PreferencesController::class , 'loginUpdate'])->name('login-update');

            Route::get('/payment' , [PreferencesController::class , 'payment'])->name('payment');
            Route::post('/payment/update/{preferences}' , [PreferencesController::class , 'paymentUpdate'])->name('payment-update');

            Route::get('/web' , [PreferencesController::class , 'web'])->name('web');
            Route::post('/web/update/{preferences}' , [PreferencesController::class , 'webUpdate'])->name('web-update');

            Route::post('/speaker/create' , [PreferencesController::class , 'speakerInsert'])->name('speaker.create');
            Route::get('/speaker/delete/{speaker}' , [PreferencesController::class , 'speakerDelete'])->name('speaker.delete');
        });

        Route::group(['prefix' => 'banner' , 'as' => 'banner.'], function (){
            Route::get('/image', [BannerController::class , 'image'])->name('image');
            Route::get('/youtube', [BannerController::class , 'youtube'])->name('youtube');
            Route::get('/create' ,[BannerController::class , 'create'])->name('create');
            Route::post('/store' ,[BannerController::class , 'store'])->name('store');
            Route::get('/{banner}' ,[BannerController::class , 'destroy'])->name('delete');
            Route::get('/update/{banner}' ,[BannerController::class , 'edit'])->name('edit');
            Route::post('/edit/{banner}' ,[BannerController::class , 'update'])->name('update');
            
        });

        Route::group(['prefix' => 'participant' , 'as' => 'participant.'], function (){
            Route::get('/web-regisration', [ParticipantController::class , 'web'])->name('web-regis');
            Route::get('/invitation', [ParticipantController::class , 'invite'])->name('invite');
            Route::get('/create' ,[ParticipantController::class , 'create'])->name('create');
            Route::post('/store' ,[ParticipantController::class , 'store'])->name('store');
            Route::get('/detail/{participant}', [ParticipantController::class , 'show'])->name('show');
            Route::get('/{participant}' ,[ParticipantController::class , 'destroy'])->name('delete');
            Route::get('/update/{participant}' ,[ParticipantController::class , 'edit'])->name('edit');
            Route::post('/edit/{participant}' ,[ParticipantController::class , 'update'])->name('update');
            
        });
        
        Route::group(['prefix' => 'ticket' , 'as' => 'ticket.'], function () {
            Route::get('/', [TicketController::class , 'index'])->name('index');
            Route::get('/create' ,[TicketController::class , 'create'])->name('create');
            Route::post('/store' ,[TicketController::class , 'store'])->name('store');
            Route::get('/{ticket}' ,[TicketController::class , 'delete'])->name('delete');
            Route::get('/update/{ticket}' ,[TicketController::class , 'edit'])->name('edit');
            Route::post('/edit/{ticket}' ,[TicketController::class , 'update'])->name('update');
        });

        Route::group(['prefix' => 'notifications' , 'as' => 'notif.'], function () {
            Route::get('/', [NotifikasiController::class , 'index'])->name('index');
            Route::get('/create' ,[NotifikasiController::class , 'create'])->name('create');
            Route::post('/store' ,[NotifikasiController::class , 'store'])->name('store');
            Route::get('/{notif}' ,[NotifikasiController::class , 'delete'])->name('delete');
            Route::get('/update/{notif}' ,[NotifikasiController::class , 'edit'])->name('edit');
            Route::post('/edit/{notif}' ,[NotifikasiController::class , 'update'])->name('update');
        });

        Route::group(['prefix' => 'booth' , 'as' => 'booth.'], function (){
            Route::get('/', [BoothController::class , 'index'])->name('index');
            Route::get('/create' ,[BoothController::class , 'create'])->name('create');
            Route::post('/store' ,[BoothController::class , 'store'])->name('store');
            Route::get('/{booth}' ,[BoothController::class , 'destroy'])->name('delete');
            Route::get('/update/{booth}' ,[BoothController::class , 'edit'])->name('edit');
            Route::post('/edit/{booth}' ,[BoothController::class , 'update'])->name('update');
        });

        Route::group(['prefix' => 'booth' , 'as' => 'booth.'], function (){
            Route::get('/', [BoothController::class , 'index'])->name('index');
            Route::get('/create' ,[BoothController::class , 'create'])->name('create');
            Route::post('/store' ,[BoothController::class , 'store'])->name('store');
            Route::get('/{booth}' ,[BoothController::class , 'destroy'])->name('delete');
            Route::get('/update/{booth}' ,[BoothController::class , 'edit'])->name('edit');
            Route::post('/edit/{booth}' ,[BoothController::class , 'update'])->name('update');
        });

        Route::group(['prefix' => 'banner' , 'as' => 'banner.'], function (){
            Route::get('/image', [BannerController::class , 'image'])->name('image');
            Route::get('/youtube', [BannerController::class , 'youtube'])->name('youtube');
            Route::get('/create' ,[BannerController::class , 'create'])->name('create');
            Route::post('/store' ,[BannerController::class , 'store'])->name('store');
            Route::get('/{banner}' ,[BannerController::class , 'destroy'])->name('delete');
            Route::get('/update/{banner}' ,[BannerController::class , 'edit'])->name('edit');
            Route::post('/edit/{banner}' ,[BannerController::class , 'update'])->name('update');

        });

        Route::group(['prefix' => 'ticket' , 'as' => 'ticket.'], function () {
            Route::get('/', [TicketController::class , 'index'])->name('index');
            Route::get('/create' ,[TicketController::class , 'create'])->name('create');
            Route::post('/store' ,[TicketController::class , 'store'])->name('store');
            Route::get('/{ticket}' ,[TicketController::class , 'delete'])->name('delete');
            Route::get('/update/{ticket}' ,[TicketController::class , 'edit'])->name('edit');
            Route::post('/edit/{ticket}' ,[TicketController::class , 'update'])->name('update');
            Route::get('/schedule/{ticket}' ,[TicketController::class , 'schedule'])->name('schedule');
            Route::get('/schedule-delete/{ticket}' ,[TicketController::class , 'scheduleDelete'])->name('schedule.delete');
        });

        Route::group(['prefix' => 'schedule' , 'as' => 'schedule.'], function () {
            /* Main Stage */
            Route::group(['prefix' => 'main-stage' , 'as' => 'main_stage.'], function () {
                Route::get('/', [MainStageController::class , 'index'])->name('index');
                Route::get('/create', [MainStageController::class , 'create'])->name('create');
                Route::post('/store', [MainStageController::class , 'store'])->name('store');
                Route::get('/{main_stage}/edit', [MainStageController::class , 'edit'])->name('edit');
                Route::post('/{main_stage}/update', [MainStageController::class , 'update'])->name('update');
                Route::get('/{main_stage}', [MainStageController::class , 'delete'])->name('delete');
                Route::get('/{main_stage}/attachment', [MainStageController::class , 'attachment'])->name('attachment');
                Route::get('/{main_stage}/attachment/create', [MainStageController::class , 'attachmentCreate'])->name('attachment.create');
                Route::post('/{main_stage}/attachment/store', [MainStageController::class , 'attachmentStore'])->name('attachment.store');
                Route::get('/{main_stage}/attachment/edit', [MainStageController::class , 'attachmentEdit'])->name('attachment.edit');
                Route::post('/{main_stage}/attachment/update', [MainStageController::class , 'attachmentUpdate'])->name('attachment.update');
                Route::get('/{main_stage}/attachment/delete', [MainStageController::class , 'attachmentDelete'])->name('attachment.delete');
            });

            /* Classroom */
            Route::group(['prefix' => 'classroom' , 'as' => 'classroom.'], function () {
                Route::get('/', [ClassroomController::class , 'index'])->name('index');
                Route::get('/create', [ClassroomController::class , 'create'])->name('create');
                Route::post('/store', [ClassroomController::class , 'store'])->name('store');
                Route::get('/{classroom}/edit', [ClassroomController::class , 'edit'])->name('edit');
                Route::post('/{classroom}/update', [ClassroomController::class , 'update'])->name('update');
                Route::get('/{classroom}', [ClassroomController::class , 'delete'])->name('delete');
                Route::get('/{classroom}/attachment', [ClassroomController::class , 'attachment'])->name('attachment');
                Route::get('/{classroom}/attachment/create', [ClassroomController::class , 'attachmentCreate'])->name('attachment.create');
                Route::post('/{classroom}/attachment/store', [ClassroomController::class , 'attachmentStore'])->name('attachment.store');
                Route::get('/{classroom}/attachment/edit', [ClassroomController::class , 'attachmentEdit'])->name('attachment.edit');
                Route::post('/{classroom}/attachment/update', [ClassroomController::class , 'attachmentUpdate'])->name('attachment.update');
                Route::get('/{classroom}/attachment/delete', [ClassroomController::class , 'attachmentDelete'])->name('attachment.delete');
            });
        });

        Route::group(['prefix' => 'payments' , 'as' => 'payments.'], function () {
            Route::group(['prefix' => 'history' , 'as' => 'history.'], function () {
                Route::get('/', [PaymentHistoryController::class , 'index'])->name('index');
                Route::get('/detail/{payment}', [PaymentHistoryController::class , 'detail'])->name('detail');
                Route::get('/{payment}/detail', [PaymentHistoryController::class , 'data'])->name('lihat');
                Route::get('/{payment}/confirmation', [PaymentHistoryController::class , 'confirmation'])->name('confirmation');
            });

            Route::group(['prefix' => 'confirmation' , 'as' => 'confirmation.'], function () {
                Route::get('/', [PaymentConfirmationController::class , 'index'])->name('index');
                Route::get('/approved/{payment}', [PaymentConfirmationController::class , 'approved'])->name('approved');
                Route::get('/rejected/{payment}', [PaymentConfirmationController::class , 'rejected'])->name('rejected');
            });
        });

        Route::group(['prefix' => 'sponsor' , 'as' => 'sponsor.'], function (){
            Route::get('/gold', [SponsorController::class , 'gold'])->name('gold');
            Route::get('/silver', [SponsorController::class , 'silver'])->name('silver');
            Route::get('/bronze', [SponsorController::class , 'bronze'])->name('bronze');
            Route::get('/create' ,[SponsorController::class , 'create'])->name('create');
            Route::post('/store' ,[SponsorController::class , 'store'])->name('store');
            Route::get('/{sponsor}' ,[SponsorController::class , 'destroy'])->name('delete');
            Route::get('/update/{sponsor}' ,[SponsorController::class , 'edit'])->name('edit');
            Route::post('/edit/{sponsor}' ,[SponsorController::class , 'update'])->name('update');

        });

        Route::group(['prefix' => 'polling' , 'as' => 'polling.'], function () {
            Route::get('/', [PollingController::class , 'index'])->name('index');
            Route::get('/create' ,[PollingController::class , 'create'])->name('create');
            Route::post('/store' ,[PollingController::class , 'store'])->name('store');
            Route::get('/{polling}' ,[PollingController::class , 'delete'])->name('delete');
            Route::get('/update/{polling}' ,[PollingController::class , 'edit'])->name('edit');
            Route::post('/edit/{polling}' ,[PollingController::class , 'update'])->name('update');
        });

    });
});

